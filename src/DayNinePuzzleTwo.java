import support.LinkedListItem;

public class DayNinePuzzleTwo {

    static String input = "446 players; last marble is worth 7152200 points";

    //static String input = "10 players; last marble is worth 1618 points: high score is 8317";
    //static String input = "13 players; last marble is worth 7999 points: high score is 146373";
    //static String input = "17 players; last marble is worth 1104 points: high score is 2764";
    //static String input = "21 players; last marble is worth 6111 points: high score is 54718";
    //static String input = "30 players; last marble is worth 5807 points: high score is 37305";
    //static String input = "9 players; last marble is worth 25 points: high score is 32";

    public static void main(String[] args){
        int numPlayers = Integer.parseInt(input.substring(0, input.indexOf(" ")));
        int numMarbles = Integer.parseInt(input.substring(input.indexOf("worth")+6, input.indexOf(" points")));

        long[] scores = new long[numPlayers];
        LinkedListItem current = new LinkedListItem(0);
        current.setNext(current);
        current.setPrevious(current);
        current = addLinkedListItemAfter(current, 1);

        for(int i=2;i<=numMarbles;i++){
            if(i%23==0){
                scores[(i-1)%numPlayers]+=i;
                LinkedListItem itemToRemove = current.getPrevious().getPrevious().getPrevious().getPrevious().getPrevious().getPrevious().getPrevious();
                scores[(i-1)%numPlayers]+=itemToRemove.getMarble();
                current = removeItem(itemToRemove);
            }
            else{
                current = addLinkedListItemAfter(current.getNext(), i);
            }
        }
        long max = -1;
        for(int i=0;i<numPlayers;i++)
            if(scores[i] > max)
                max = scores[i];
        System.out.println(max);
    }

    private static LinkedListItem addLinkedListItemAfter(LinkedListItem current, int marble){
        LinkedListItem next = current.getNext();
        LinkedListItem newItem = new LinkedListItem(marble);
        newItem.setPrevious(current);
        newItem.setNext(next);
        current.setNext(newItem);
        next.setPrevious(newItem);
        return newItem;
    }

    private static LinkedListItem removeItem(LinkedListItem itemToRemove){
        LinkedListItem previous = itemToRemove.getPrevious();
        LinkedListItem next = itemToRemove.getNext();
        next.setPrevious(previous);
        previous.setNext(next);
        return next;
    }
}
