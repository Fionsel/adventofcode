import support.CaveType;

import java.util.PriorityQueue;

public class DayTwentyTwoPuzzleTwo {
    static int depth = 10914;
    static int targetx = 9;
    static int targety = 739;
    static int extrax = 50;
    static int extray = 50;
    static int xfactor = 16807;
    static int yfactor = 48271;
    static int erosionLevelIndex = 20183;
    static int[][] type;
    static PriorityQueue<CaveType> queue;
    static Integer[][][] visited;

    //0 = rocky, 1 = wet, 2 = narrow
    //rocky = climbing gear or torch
    //wet = climbing gear or nothing
    //narrow = torch or nothing
    //0 = nothing, 1 = torch, 2 = climbing gear
    public static void main(String[] args){
        int[][] erosionLevel = new int[targety+extray+1][targetx+extrax+1];
        for(int j=0;j<targetx+extrax+1;j++)erosionLevel[0][j]=(xfactor*j+depth)%erosionLevelIndex;
        for(int i=0;i<targety+extray+1;i++)erosionLevel[i][0]=(yfactor*i+depth)%erosionLevelIndex;
        for(int i=1;i<targety+extray+1;i++)for(int j=1;j<targetx+extrax+1;j++)erosionLevel[i][j]=(i==targety && j==targety)?depth:(erosionLevel[i][j-1]*erosionLevel[i-1][j]+depth)%erosionLevelIndex;
        type = new int[targety+extray+1][targetx+extrax+1];
        for(int i=0;i<type.length;i++)for(int j=0;j<type[0].length;j++)type[i][j]=(erosionLevel[i][j]%3);
        queue = new PriorityQueue<>();
        queue.add(new CaveType(0,0,1, 0, 0));
        CaveType caveType;
        visited = new Integer[3][type.length][type[0].length];
        visited[1][0][0]=0;
        while(true){
            caveType = queue.poll();
            if(caveType.getX()==targetx && caveType.getY()==targety&&caveType.getEquipment()==1) break;

            int newEquipment = 3-caveType.getType()-caveType.getEquipment();
            if(caveType.getX()>0){
                performStep(caveType.getX()-1, caveType.getY(), caveType.getEquipment(),caveType.getDuration()+1);
                performStep(caveType.getX()-1, caveType.getY(), newEquipment,caveType.getDuration()+8);
            }
            if(caveType.getY()>0){
                performStep(caveType.getX(), caveType.getY()-1, caveType.getEquipment(), caveType.getDuration()+1);
                performStep(caveType.getX(), caveType.getY()-1, newEquipment,caveType.getDuration()+8);
            }
            if(caveType.getX()<type[0].length-1){
                performStep(caveType.getX()+1, caveType.getY(), caveType.getEquipment(), caveType.getDuration()+1);
                performStep(caveType.getX()+1, caveType.getY(), newEquipment, caveType.getDuration()+8);
            }
            if(caveType.getY()<type.length-1){
                performStep(caveType.getX(), caveType.getY()+1, caveType.getEquipment(), caveType.getDuration()+1);
                performStep(caveType.getX(), caveType.getY()+1, newEquipment, caveType.getDuration()+8);
            }
        }
        System.out.println(caveType.getDuration());
    }

    private static void performStep(int newX, int newY, int equipment, int duration){
        int nextType = type[newY][newX];
        if(equipment != nextType && (visited[equipment][newY][newX] == null || visited[equipment][newY][newX] > duration)){
            queue.add(new CaveType(duration, nextType, equipment, newX, newY));
            visited[equipment][newY][newX] = duration;
        }
    }
}
