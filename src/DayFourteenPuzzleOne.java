import support.Recipe;

public class DayFourteenPuzzleOne {

    static int input = 990941;

    public static void main(String[] args){
        Recipe firstRecipe = new Recipe(3, 1);
        Recipe lastRecipe = new Recipe(7, 2);
        addRecipeToEnd(firstRecipe, lastRecipe);
        Recipe firstElf = firstRecipe;
        Recipe secondElf = lastRecipe;
        int time = 0;
        while(lastRecipe.getIndex() < input+10){
            int sum = firstElf.getScore()+secondElf.getScore();
            if(sum < 10){
                Recipe newRecipe = new Recipe(sum, lastRecipe.getIndex()+1);
                addRecipeToEnd(lastRecipe, newRecipe);
                lastRecipe = newRecipe;
            } else {
                Recipe newRecipe = new Recipe(sum/10, lastRecipe.getIndex()+1);
                addRecipeToEnd(lastRecipe, newRecipe);
                lastRecipe = newRecipe;
                newRecipe = new Recipe(sum%10, lastRecipe.getIndex()+1);
                addRecipeToEnd(lastRecipe, newRecipe);
                lastRecipe = newRecipe;
            }
            int steps = 1+firstElf.getScore();
            for(int i=0;i<steps;i++)
                firstElf = firstElf.getNext();
            steps = 1+secondElf.getScore();
            for(int i=0;i<steps;i++)
                secondElf = secondElf.getNext();
            //time++;
            //System.out.println("time "+time+" fe "+firstElf.getScore()+" se "+secondElf.getScore());
        }
        while(lastRecipe.getIndex() > input)
            lastRecipe = lastRecipe.getPrevious();

        for(int i=0;i<10;i++) {
            lastRecipe = lastRecipe.getNext();
            System.out.print(lastRecipe.getScore());
        }
        System.out.println();

        /*support.Recipe currentRecipe = firstRecipe;
        do{
            System.out.print(currentRecipe.getScore());
            currentRecipe = currentRecipe.getNext();
        }while(currentRecipe.getNext() != firstRecipe);*/

    }

    private static void addRecipeToEnd(Recipe lastRecipe, Recipe newRecipe){
        newRecipe.setPrevious(lastRecipe);
        newRecipe.setNext(lastRecipe.getNext());
        lastRecipe.setNext(newRecipe);
    }
}
