import support.Node;

import java.util.Collections;
import java.util.Vector;

public class DaySevenPuzzleOne {
    
    static String[] input = new String[]{
            "Step Y must be finished before step A can begin." ,
                    "Step O must be finished before step C can begin." ,
                    "Step P must be finished before step A can begin." ,
                    "Step D must be finished before step N can begin." ,
                    "Step T must be finished before step G can begin." ,
                    "Step L must be finished before step M can begin." ,
                    "Step X must be finished before step V can begin." ,
                    "Step C must be finished before step R can begin." ,
                    "Step G must be finished before step E can begin." ,
                    "Step H must be finished before step N can begin." ,
                    "Step Q must be finished before step B can begin." ,
                    "Step S must be finished before step R can begin." ,
                    "Step M must be finished before step F can begin." ,
                    "Step N must be finished before step Z can begin." ,
                    "Step E must be finished before step I can begin." ,
                    "Step A must be finished before step R can begin." ,
                    "Step Z must be finished before step F can begin." ,
                    "Step K must be finished before step V can begin." ,
                    "Step I must be finished before step J can begin." ,
                    "Step R must be finished before step W can begin." ,
                    "Step B must be finished before step J can begin." ,
                    "Step W must be finished before step V can begin." ,
                    "Step V must be finished before step F can begin." ,
                    "Step U must be finished before step F can begin." ,
                    "Step F must be finished before step J can begin." ,
                    "Step X must be finished before step C can begin." ,
                    "Step T must be finished before step Q can begin." ,
                    "Step B must be finished before step F can begin." ,
                    "Step Y must be finished before step L can begin." ,
                    "Step P must be finished before step E can begin." ,
                    "Step A must be finished before step J can begin." ,
                    "Step S must be finished before step I can begin." ,
                    "Step S must be finished before step A can begin." ,
                    "Step K must be finished before step R can begin." ,
                    "Step D must be finished before step C can begin." ,
                    "Step R must be finished before step U can begin." ,
                    "Step K must be finished before step U can begin." ,
                    "Step D must be finished before step K can begin." ,
                    "Step S must be finished before step M can begin." ,
                    "Step D must be finished before step E can begin." ,
                    "Step A must be finished before step K can begin." ,
                    "Step G must be finished before step I can begin." ,
                    "Step O must be finished before step M can begin." ,
                    "Step U must be finished before step J can begin." ,
                    "Step T must be finished before step S can begin." ,
                    "Step C must be finished before step M can begin." ,
                    "Step S must be finished before step J can begin." ,
                    "Step N must be finished before step V can begin." ,
                    "Step P must be finished before step N can begin." ,
                    "Step D must be finished before step M can begin." ,
                    "Step A must be finished before step B can begin." ,
                    "Step Z must be finished before step R can begin." ,
                    "Step T must be finished before step N can begin." ,
                    "Step K must be finished before step J can begin." ,
                    "Step N must be finished before step A can begin." ,
                    "Step M must be finished before step R can begin." ,
                    "Step E must be finished before step A can begin." ,
                    "Step Y must be finished before step O can begin." ,
                    "Step O must be finished before step B can begin." ,
                    "Step O must be finished before step A can begin." ,
                    "Step I must be finished before step V can begin." ,
                    "Step M must be finished before step Z can begin." ,
                    "Step D must be finished before step U can begin." ,
                    "Step O must be finished before step S can begin." ,
                    "Step Z must be finished before step W can begin." ,
                    "Step M must be finished before step A can begin." ,
                    "Step N must be finished before step E can begin." ,
                    "Step M must be finished before step U can begin." ,
                    "Step R must be finished before step J can begin." ,
                    "Step W must be finished before step F can begin." ,
                    "Step I must be finished before step U can begin." ,
                    "Step E must be finished before step U can begin." ,
                    "Step Y must be finished before step R can begin." ,
                    "Step Z must be finished before step K can begin." ,
                    "Step C must be finished before step F can begin." ,
                    "Step B must be finished before step V can begin." ,
                    "Step G must be finished before step B can begin." ,
                    "Step O must be finished before step G can begin." ,
                    "Step E must be finished before step Z can begin." ,
                    "Step A must be finished before step V can begin." ,
                    "Step Y must be finished before step Q can begin." ,
                    "Step P must be finished before step D can begin." ,
                    "Step X must be finished before step G can begin." ,
                    "Step I must be finished before step W can begin." ,
                    "Step M must be finished before step V can begin." ,
                    "Step T must be finished before step M can begin." ,
                    "Step G must be finished before step J can begin." ,
                    "Step T must be finished before step I can begin." ,
                    "Step H must be finished before step B can begin." ,
                    "Step C must be finished before step E can begin." ,
                    "Step Q must be finished before step V can begin." ,
                    "Step H must be finished before step U can begin." ,
                    "Step X must be finished before step K can begin." ,
                    "Step D must be finished before step T can begin." ,
                    "Step X must be finished before step W can begin." ,
                    "Step P must be finished before step Z can begin." ,
                    "Step C must be finished before step U can begin." ,
                    "Step Y must be finished before step Z can begin." ,
                    "Step L must be finished before step F can begin." ,
                    "Step C must be finished before step J can begin." ,
                    "Step T must be finished before step W can begin."
    };
    
    /*static String[] input = new String[]{
            "Step C must be finished before step A can begin." ,
                    "Step C must be finished before step F can begin." ,
                    "Step A must be finished before step B can begin." ,
                    "Step A must be finished before step D can begin." ,
                    "Step B must be finished before step E can begin." ,
                    "Step D must be finished before step E can begin." ,
                    "Step F must be finished before step E can begin."
    };*/

    public static void main(String[] args){
        Node[] nodes = new Node[26];
        for(String message : input){
            String nodeName = message.substring(36,37);
            int position = nodeName.charAt(0)-'A';
            if(nodes[position] == null){
                nodes[position] = new Node(nodeName);
            }
            String dependency = message.substring(5,6);
            nodes[position].getDependencies().add(dependency);

            position = dependency.charAt(0)-'A';
            if(nodes[position] == null){
                nodes[position] = new Node(dependency);
            }
        }
        String[] order = new String[26];
        Vector<String> usedNodes = new Vector<>();
        for(int i=0;i<26;i++){
            String next = getNextAvailableNode(nodes, usedNodes);
            if(next == null)break;

            order[i] = next;
            usedNodes.add(next);
            int position = next.charAt(0)-'A';
            nodes[position] = null;
        }
        for(String next : order)
            System.out.print(next);
    }

    private static String getNextAvailableNode(Node[] nodes, Vector<String> usedNodes){
        Vector<String> possibleNodes = new Vector<>();
        for(int i=0;i<26;i++){
            Node node = nodes[i];
            if(node == null)continue;

            boolean allDepenciesResolved = true;
            for(String dependecy : node.getDependencies()){
                if(!usedNodes.contains(dependecy)){
                    allDepenciesResolved = false;
                    break;
                }
            }
            if(allDepenciesResolved){
                possibleNodes.add(node.getName());
            }
        }
        Collections.sort(possibleNodes);
        return possibleNodes.size() > 0 ? possibleNodes.firstElement() : null;
    }
}
