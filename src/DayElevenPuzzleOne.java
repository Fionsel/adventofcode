public class DayElevenPuzzleOne {

    static int serialNumber = 9424;
    //static int serialNumber = 18;

    static int gridSize = 300;
    static int[][] fuel = new int[gridSize][gridSize];

    public static void main(String[] args){

        for(int i=0;i<gridSize;i++){
            for(int j=0;j<gridSize;j++){
                int rackId = i+11;
                int powerLevel = rackId * (j+1);
                int total = (powerLevel+serialNumber)*rackId;
                int hundredDigit = (total%1000)/100;
                fuel[i][j] = hundredDigit-5;
            }
        }

        int maxValue = -100000;
        int maxX = -1, maxY = -1, maxK = -1;
        for(int k=1;k<=300;k++) {
            for (int i = 0; i < gridSize - k + 1; i++) {
                for (int j = 0; j < gridSize - k + 1; j++) {
                    int total = totalValueForCell(i, j, k);
                    if (total > maxValue) {
                        maxValue = total;
                        maxX = i+1;
                        maxY = j+1;
                        maxK = k;
                    }
                }
            }
        }
        System.out.println(maxValue + " "+maxX + " "+maxY+" "+maxK);
    }

    private static int totalValueForCell(int x, int y, int k){
        int sum = 0;
        for(int i=x;i<x+k;i++){
            for(int j=y;j<y+k;j++){
                sum += fuel[i][j];
            }
        }
        return sum;
    }
}
