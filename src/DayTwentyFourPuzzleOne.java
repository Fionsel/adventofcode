import support.Group;

import java.util.List;
import java.util.Vector;
import java.util.stream.Collectors;

public class DayTwentyFourPuzzleOne {
    static String[] immuneSystemInput = new String[]{
        "76 units each with 3032 hit points with an attack that does 334 radiation damage at initiative 7",
        "4749 units each with 8117 hit points with an attack that does 16 bludgeoning damage at initiative 16",
        "4044 units each with 1287 hit points (immune to radiation, fire) with an attack that does 2 fire damage at initiative 20",
        "1130 units each with 11883 hit points (weak to radiation) with an attack that does 78 radiation damage at initiative 14",
        "1698 units each with 2171 hit points (weak to slashing, fire) with an attack that does 11 bludgeoning damage at initiative 12",
        "527 units each with 1485 hit points with an attack that does 26 bludgeoning damage at initiative 17",
        "2415 units each with 4291 hit points (immune to radiation) with an attack that does 17 cold damage at initiative 5",
        "3266 units each with 6166 hit points (immune to cold, slashing; weak to radiation) with an attack that does 17 bludgeoning damage at initiative 18",
        "34 units each with 8390 hit points (immune to cold, fire, slashing) with an attack that does 2311 cold damage at initiative 10",
        "3592 units each with 5129 hit points (immune to cold, fire; weak to radiation) with an attack that does 14 radiation damage at initiative 11"
    };

    static String[] infectionInput = new String[]{            
        "3748 units each with 11022 hit points (weak to bludgeoning) with an attack that does 4 bludgeoning damage at initiative 6",
        "2026 units each with 11288 hit points (weak to fire, slashing) with an attack that does 10 slashing damage at initiative 13",
        "4076 units each with 23997 hit points (immune to cold) with an attack that does 11 bludgeoning damage at initiative 19",
        "4068 units each with 40237 hit points (immune to cold; weak to slashing) with an attack that does 18 slashing damage at initiative 4",
        "3758 units each with 16737 hit points (weak to slashing) with an attack that does 6 radiation damage at initiative 2",
        "1184 units each with 36234 hit points (weak to bludgeoning, fire; immune to cold) with an attack that does 60 radiation damage at initiative 1",
        "1297 units each with 36710 hit points (immune to cold) with an attack that does 47 fire damage at initiative 3",
        "781 units each with 18035 hit points (immune to bludgeoning, slashing) with an attack that does 36 fire damage at initiative 15",
        "1491 units each with 46329 hit points (immune to slashing, bludgeoning) with an attack that does 56 fire damage at initiative 8",
        "1267 units each with 34832 hit points (immune to cold) with an attack that does 49 radiation damage at initiative 9"
    };
    
    /*static String[] immuneSystemInput = new String[]{
        "17 units each with 5390 hit points (weak to radiation, bludgeoning) with an attack that does 4507 fire damage at initiative 2",
        "989 units each with 1274 hit points (immune to fire; weak to bludgeoning, slashing) with an attack that does 25 slashing damage at initiative 3",
    };

    static String[] infectionInput = new String[]{
            "801 units each with 4706 hit points (weak to radiation) with an attack that does 116 bludgeoning damage at initiative 1",
            "4485 units each with 2961 hit points (immune to radiation; weak to fire, cold) with an attack that does 12 slashing damage at initiative 4"
    };*/

    static Vector<Group> immunities = new Vector<>();
    static Vector<Group> infections = new Vector<>();

    static int immuneSystemBoost = 119;

    public static void main(String[] args){
        int counter = 0;
        for(String input : immuneSystemInput){
            immunities.add(createGroupFromInput(input, true, ++counter, immuneSystemBoost));
        }
        counter = 0;
        for(String input : infectionInput){
            infections.add(createGroupFromInput(input, false, ++counter, 0));
        }
        List<Group> allGroups = new Vector<>();
        allGroups.addAll(infections);
        allGroups.addAll(immunities);
        int round = 0;
        while(immunities.stream().anyMatch(i -> i.getSize() > 0) && infections.stream().anyMatch(i -> i.getSize() > 0)){

            round++;
            //reset phase
            allGroups = allGroups.stream().sorted(Group::compareTo).collect(Collectors.toList());
            for (Group group : allGroups){
                group.setTarget(false);
            }

            //System.out.println("Round "+round);
            //selection phase
            for (Group group : allGroups){
                if(group.getSize() > 0) {
                    group.determineTarget(group.isImmunity() ? infections : immunities);
                    //if(group.getTarget()!=null)System.out.println((group.isImmunity() ? "Immunity" : "Infection")+group.getCounter()+" has chosen target "+(group.getTarget().isImmunity() ? "Immunity" : "Infection")+group.getTarget().getCounter());
                    //else System.out.println((group.isImmunity() ? "Immunity" : "Infection")+group.getCounter()+" has chosen no target");
                }
            }
            //attack phase
            allGroups = allGroups.stream().sorted(Group::compareToAttack).collect(Collectors.toList());
            for (Group group : allGroups){
                if(group.getSize() > 0) {
                    group.attackTarget();
                    //if(group.getTarget()!=null)System.out.println((group.isImmunity() ? "Immunity" : "Infection")+group.getCounter()+" has attacked target "+(group.getTarget().isImmunity() ? "Immunity" : "Infection")+group.getTarget().getCounter());
                    //else System.out.println((group.isImmunity() ? "Immunity" : "Infection")+group.getCounter()+" has attacked no target");
                }
            }
            //System.out.println("End of found");
            //System.out.println();
        }
        System.out.println(immunities.stream().anyMatch(g -> g.getSize() > 0));
        System.out.println(allGroups.stream().filter(g -> g.getSize()>0).map(Group::getSize).mapToInt(i -> i).sum());
    }

    private static Group createGroupFromInput(String input, boolean isImmunity, int counter, int damageBoost){
        int units = Integer.parseInt(input.substring(0, input.indexOf(" unit")));
        int hitPoints = Integer.parseInt(input.substring(input.indexOf("each with ")+"each with ".length(), input.indexOf(" hit points")));
        int damageIndex = input.indexOf("that does ")+"that does ".length();
        int damage = Integer.parseInt(input.substring(damageIndex, input.indexOf(" ",damageIndex)))+damageBoost;
        int damageTypeIndex = input.indexOf(" ",damageIndex)+1;
        String damageType = input.substring(damageTypeIndex,input.indexOf(" damage at "));
        int initiative = Integer.parseInt(input.substring(input.indexOf("at initiative ")+"at initiative ".length()));
        Group group = new Group(hitPoints, units, damage, damageType, initiative, isImmunity, counter);
        if(input.contains("(")){
            String[] types = input.substring(input.indexOf("(")+1, input.indexOf(")")).split("; ");
            for (String type : types){
                if(type.startsWith("immune to "))
                    for (String immunityType : type.substring(type.indexOf("immune to ")+"immune to ".length()).split(", "))
                        group.getImmunities().add(immunityType);
                if(type.startsWith("weak to "))
                    for (String weakness : type.substring(type.indexOf("weak to ")+"weak to ".length()).split(", "))
                        group.getWeaknessses().add(weakness);
            }
        }
        return group;
    }
}
