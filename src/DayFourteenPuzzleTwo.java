import support.Recipe;

public class DayFourteenPuzzleTwo {
    static int input = 990941;

    public static void main(String[] args){
        Recipe firstRecipe = new Recipe(3, 1);
        Recipe lastRecipe = new Recipe(7, 2);
        addRecipeToEnd(firstRecipe, lastRecipe);
        Recipe firstElf = firstRecipe;
        Recipe secondElf = lastRecipe;
        Recipe numberReached = null;
        //int time=0;
        while(numberReached == null){
            int sum = firstElf.getScore()+secondElf.getScore();
            if(sum < 10){
                Recipe newRecipe = new Recipe(sum, lastRecipe.getIndex()+1);
                addRecipeToEnd(lastRecipe, newRecipe);
                lastRecipe = newRecipe;
                if(isNumberReached(lastRecipe, input)) {
                    numberReached = lastRecipe;
                    break;
                }
            } else {
                Recipe newRecipe = new Recipe(sum/10, lastRecipe.getIndex()+1);
                addRecipeToEnd(lastRecipe, newRecipe);
                lastRecipe = newRecipe;
                if(isNumberReached(lastRecipe, input)) {
                    numberReached = lastRecipe;
                    break;
                }
                newRecipe = new Recipe(sum%10, lastRecipe.getIndex()+1);
                addRecipeToEnd(lastRecipe, newRecipe);
                lastRecipe = newRecipe;
                if(isNumberReached(lastRecipe, input)) {
                    numberReached = lastRecipe;
                    break;
                }
            }
            int steps = 1+firstElf.getScore();
            for(int i=0;i<steps;i++)
                firstElf = firstElf.getNext();
            steps = 1+secondElf.getScore();
            for(int i=0;i<steps;i++)
                secondElf = secondElf.getNext();
            //time++;
            //if(time==100) numberReached = firstElf;
        }

        if(input > 9) {
            int temp = input;
            while (temp / 10 > 0) {
                numberReached = numberReached.getPrevious();
                temp /= 10;
            }
        }

        /*String result = "";
        for(int i=0;i<5;i++) {
            result += (numberReached.getScore());
            numberReached = numberReached.getPrevious();
        }
        System.out.println(new StringBuilder(result).reverse().toString());

        support.Recipe currentRecipe = firstRecipe;
        do{
            System.out.print(currentRecipe.getScore());
            currentRecipe = currentRecipe.getNext();
        }while(currentRecipe.getNext() != firstRecipe);*/
        System.out.print(numberReached.getPrevious().getIndex());
    }

    private static void addRecipeToEnd(Recipe lastRecipe, Recipe newRecipe){
        newRecipe.setPrevious(lastRecipe);
        newRecipe.setNext(lastRecipe.getNext());
        lastRecipe.setNext(newRecipe);
    }

    private static boolean isNumberReached(Recipe current, int number){
        if(number == 0) return true;
        return number%10 == current.getScore() && isNumberReached(current.getPrevious(), number/10);
    }
}
