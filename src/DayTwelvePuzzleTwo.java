public class DayTwelvePuzzleTwo {

    static String initialState = "#.#..#.##.#..#.#..##.######...####.........#..##...####.#.###......#.#.##..#.#.###.#..#.#.####....##";

    //static String initialState = "#..#.#..##......###...###";

    static String[] transformations = new String[]{
            ".#### => ." ,
            ".###. => ." ,
            "#.... => ." ,
            "##### => ." ,
            "..### => #" ,
            "####. => #" ,
            "..#.. => ." ,
            "###.# => ." ,
            "..##. => ." ,
            "#.##. => #" ,
            "#.#.. => ." ,
            "##... => ." ,
            "..#.# => #" ,
            "#.### => #" ,
            ".#..# => ." ,
            "#...# => #" ,
            ".##.# => #" ,
            ".#.#. => #" ,
            "#..#. => #" ,
            "###.. => #" ,
            "...#. => ." ,
            ".#.## => #" ,
            ".##.. => ." ,
            "#..## => ." ,
            "##.## => ." ,
            ".#... => #" ,
            "#.#.# => ." ,
            "##..# => ." ,
            "....# => ." ,
            "..... => ." ,
            "...## => #" ,
            "##.#. => ."
    };

    /*static String[] transformations = new String[]{
            "...## => #" ,
                    "..#.. => #" ,
                    ".#... => #" ,
                    ".#.#. => #" ,
                    ".#.## => #" ,
                    ".##.. => #" ,
                    ".#### => #" ,
                    "#.#.# => #" ,
                    "#.### => #" ,
                    "##.#. => #" ,
                    "##.## => #" ,
                    "###.. => #" ,
                    "###.# => #" ,
                    "####. => #"
    };*/

    static String[] conditions = new String[transformations.length];
    static String[] outcomes = new String[transformations.length];

    public static void main(String[] args){
        setConditionsAndOutcomes();
        String hasPlant = "";
        for(int i=0;i<10;i++)
            hasPlant += ".";
        hasPlant+=initialState;
        for(int i=0;i<402;i++)
            hasPlant += ".";


        for(int j=1;j<=400;j++){
            System.out.println(hasPlant);
            String newValue = "..";
            for(int i=2;i<hasPlant.length()-2;i++){
                newValue += resultOf(hasPlant.substring(i-2,i+3));
            }
            newValue += "..";
            hasPlant = newValue;

        }
        System.out.println(hasPlant);
        for(int j=0;j<hasPlant.length();j++) {
            String subString = hasPlant.substring(j,j+1);
            if("#".equals(subString)){
                System.out.print((j-10)+" ");
            }
        }
        System.out.println();

        int sum = 0;
        System.out.println(hasPlant);
        for(int j=0;j<hasPlant.length();j++) {
            String subString = hasPlant.substring(j,j+1);
            if("#".equals(subString)){
                sum += (j-10);
            }
        }
        System.out.println(sum);
    }

    private static String resultOf(String s){
        int i=0;
        for(int k=0;k<conditions.length;k++) {
            if (conditions[k].equals(s)) {
                return outcomes[k];
            }
        }

        return ".";
    }

    private static void setConditionsAndOutcomes(){
        for(int j=0;j<transformations.length;j++){
            conditions[j] = transformations[j].substring(0,5);
            outcomes[j] = transformations[j].substring(transformations[j].length()-1);
        }
    }
}
