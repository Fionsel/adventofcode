import java.util.ArrayList;
import java.util.List;

public class DayTwentyOnePuzzleOne extends DaySixteenPuzzleOne {

    static String[] input = new String[]{        
        "seti 123 0 5",
        "bani 5 456 5",
        "eqri 5 72 5",
        "addr 5 2 2",
        "seti 0 0 2",
        "seti 0 4 5",
        "bori 5 65536 4",
        "seti 15466939 9 5",
        "bani 4 255 3",
        "addr 5 3 5",
        "bani 5 16777215 5",
        "muli 5 65899 5",
        "bani 5 16777215 5",
        "gtir 256 4 3",
        "addr 3 2 2",
        "addi 2 1 2",
        "seti 27 8 2",
        "seti 0 7 3",
        "addi 3 1 1",
        "muli 1 256 1",
        "gtrr 1 4 1",
        "addr 1 2 2",
        "addi 2 1 2",
        "seti 25 2 2",
        "addi 3 1 3",
        "seti 17 7 2",
        "setr 3 7 4",
        "seti 7 3 2",
        "eqrr 5 0 3",
        "addr 3 2 2",
        "seti 5 9 2"
    };
    
    static int registerPointer = 2;    

    public static void main(String[] args) {
        int instructionPointer = 0;
        int[] register = new int[]{0, 0, 0, 0, 0, 0};
        int counter = 0;
        int lastSeenValue = -1;
        List<Integer> seenValues = new ArrayList<>();
        while (instructionPointer >= 0 && instructionPointer < input.length) {
            counter++;
            String line = input[instructionPointer];
            register[registerPointer] = instructionPointer;
            String[] instructions = line.split(" ");
            int A = Integer.parseInt(instructions[1]);
            int B = Integer.parseInt(instructions[2]);
            int C = Integer.parseInt(instructions[3]);
            switch (instructions[0]) {
                case "addr":
                    addr(register, A, B, C);
                    break;
                case "addi":
                    addi(register, A, B, C);
                    break;
                case "setr":
                    setr(register, A, B, C);
                    break;
                case "seti":
                    seti(register, A, B, C);
                    break;
                case "muli":
                    muli(register, A, B, C);
                    break;
                case "mulr":
                    mulr(register, A, B, C);
                    break;
                case "gtir":
                    gtir(register, A, B, C);
                    break;
                case "gtrr":
                    gtrr(register, A, B, C);
                    break;
                case "gtri":
                    gtri(register, A, B, C);
                    break;
                case "eqir":
                    eqir(register, A, B, C);
                    break;
                case "eqri":
                    eqri(register, A, B, C);
                    break;
                case "eqrr":
                    if(seenValues.contains(register[5])){
                        break;
                    }
                    else{
                        seenValues.add(register[5]);
                        lastSeenValue = register[5];
                    }
                    //System.out.println(counter+" "+register[5]);
                    eqrr(register, A, B, C);
                    break;
                case "borr":
                    borr(register, A, B, C);
                    break;
                case "bori":
                    bori(register, A, B, C);
                    break;
                case "bani":
                    bani(register, A, B, C);
                    break;
                case "banr":
                    banr(register, A, B, C);
                    break;
            }
            //for(int number: register)
            //    System.out.print(number+" ");
            //System.out.println();
            /*if (++counter % 1000000 == 0) {
                for (int number : register)
                    System.out.print(number + " ");
                System.out.println();
            }*/
            instructionPointer = register[registerPointer];
            instructionPointer++;
            //System.out.println(instructionPointer);
            //System.out.println();
        }
        System.out.println(lastSeenValue);
    }
}
