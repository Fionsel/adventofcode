import support.Node;

import java.util.Collections;
import java.util.Vector;

public class DaySevenPuzzleTwo {
    static String[] input = new String[]{
            "Step Y must be finished before step A can begin." ,
            "Step O must be finished before step C can begin." ,
            "Step P must be finished before step A can begin." ,
            "Step D must be finished before step N can begin." ,
            "Step T must be finished before step G can begin." ,
            "Step L must be finished before step M can begin." ,
            "Step X must be finished before step V can begin." ,
            "Step C must be finished before step R can begin." ,
            "Step G must be finished before step E can begin." ,
            "Step H must be finished before step N can begin." ,
            "Step Q must be finished before step B can begin." ,
            "Step S must be finished before step R can begin." ,
            "Step M must be finished before step F can begin." ,
            "Step N must be finished before step Z can begin." ,
            "Step E must be finished before step I can begin." ,
            "Step A must be finished before step R can begin." ,
            "Step Z must be finished before step F can begin." ,
            "Step K must be finished before step V can begin." ,
            "Step I must be finished before step J can begin." ,
            "Step R must be finished before step W can begin." ,
            "Step B must be finished before step J can begin." ,
            "Step W must be finished before step V can begin." ,
            "Step V must be finished before step F can begin." ,
            "Step U must be finished before step F can begin." ,
            "Step F must be finished before step J can begin." ,
            "Step X must be finished before step C can begin." ,
            "Step T must be finished before step Q can begin." ,
            "Step B must be finished before step F can begin." ,
            "Step Y must be finished before step L can begin." ,
            "Step P must be finished before step E can begin." ,
            "Step A must be finished before step J can begin." ,
            "Step S must be finished before step I can begin." ,
            "Step S must be finished before step A can begin." ,
            "Step K must be finished before step R can begin." ,
            "Step D must be finished before step C can begin." ,
            "Step R must be finished before step U can begin." ,
            "Step K must be finished before step U can begin." ,
            "Step D must be finished before step K can begin." ,
            "Step S must be finished before step M can begin." ,
            "Step D must be finished before step E can begin." ,
            "Step A must be finished before step K can begin." ,
            "Step G must be finished before step I can begin." ,
            "Step O must be finished before step M can begin." ,
            "Step U must be finished before step J can begin." ,
            "Step T must be finished before step S can begin." ,
            "Step C must be finished before step M can begin." ,
            "Step S must be finished before step J can begin." ,
            "Step N must be finished before step V can begin." ,
            "Step P must be finished before step N can begin." ,
            "Step D must be finished before step M can begin." ,
            "Step A must be finished before step B can begin." ,
            "Step Z must be finished before step R can begin." ,
            "Step T must be finished before step N can begin." ,
            "Step K must be finished before step J can begin." ,
            "Step N must be finished before step A can begin." ,
            "Step M must be finished before step R can begin." ,
            "Step E must be finished before step A can begin." ,
            "Step Y must be finished before step O can begin." ,
            "Step O must be finished before step B can begin." ,
            "Step O must be finished before step A can begin." ,
            "Step I must be finished before step V can begin." ,
            "Step M must be finished before step Z can begin." ,
            "Step D must be finished before step U can begin." ,
            "Step O must be finished before step S can begin." ,
            "Step Z must be finished before step W can begin." ,
            "Step M must be finished before step A can begin." ,
            "Step N must be finished before step E can begin." ,
            "Step M must be finished before step U can begin." ,
            "Step R must be finished before step J can begin." ,
            "Step W must be finished before step F can begin." ,
            "Step I must be finished before step U can begin." ,
            "Step E must be finished before step U can begin." ,
            "Step Y must be finished before step R can begin." ,
            "Step Z must be finished before step K can begin." ,
            "Step C must be finished before step F can begin." ,
            "Step B must be finished before step V can begin." ,
            "Step G must be finished before step B can begin." ,
            "Step O must be finished before step G can begin." ,
            "Step E must be finished before step Z can begin." ,
            "Step A must be finished before step V can begin." ,
            "Step Y must be finished before step Q can begin." ,
            "Step P must be finished before step D can begin." ,
            "Step X must be finished before step G can begin." ,
            "Step I must be finished before step W can begin." ,
            "Step M must be finished before step V can begin." ,
            "Step T must be finished before step M can begin." ,
            "Step G must be finished before step J can begin." ,
            "Step T must be finished before step I can begin." ,
            "Step H must be finished before step B can begin." ,
            "Step C must be finished before step E can begin." ,
            "Step Q must be finished before step V can begin." ,
            "Step H must be finished before step U can begin." ,
            "Step X must be finished before step K can begin." ,
            "Step D must be finished before step T can begin." ,
            "Step X must be finished before step W can begin." ,
            "Step P must be finished before step Z can begin." ,
            "Step C must be finished before step U can begin." ,
            "Step Y must be finished before step Z can begin." ,
            "Step L must be finished before step F can begin." ,
            "Step C must be finished before step J can begin." ,
            "Step T must be finished before step W can begin."
    };

    /*static String[] input = new String[]{
            "Step C must be finished before step A can begin." ,
                    "Step C must be finished before step F can begin." ,
                    "Step A must be finished before step B can begin." ,
                    "Step A must be finished before step D can begin." ,
                    "Step B must be finished before step E can begin." ,
                    "Step D must be finished before step E can begin." ,
                    "Step F must be finished before step E can begin."
    };*/

    public static void main(String[] args){
        Node[] nodes = new Node[26];
        int helpers = 4;
        //int helpers = 1;
        for(String message : input){
            String nodeName = message.substring(36,37);
            int position = nodeName.charAt(0)-'A';
            if(nodes[position] == null){
                nodes[position] = new Node(nodeName);
            }
            String dependency = message.substring(5,6);
            nodes[position].getDependencies().add(dependency);

            position = dependency.charAt(0)-'A';
            if(nodes[position] == null){
                nodes[position] = new Node(dependency);
            }
        }
        Node[][] order = new Node[helpers+1][26];
        int[] positions = new int[helpers+1];
        Vector<String> usedNodes = new Vector<>();
        int time = 0;
        int total=0;
        do{
            for(int j=0;j<26;j++){
                Node node = nodes[j];
                if(node == null)continue;

                if(node.isBeingHandled() && time == (node.getStartTime()+node.getDuration())){
                    usedNodes.add(node.getName());
                }
            }
            for(int j=0;j<(helpers+1);j++){
                Node previousJob = positions[j] > 0 ? order[j][positions[j]-1] : null;
                if(previousJob != null && time < (previousJob.getDuration()+previousJob.getStartTime())) continue;

                Node next = getNextAvailableNode(nodes, usedNodes);
                if(next == null) break;

                order[j][positions[j]++] = next;
                int position = next.getName().charAt(0)-'A';
                nodes[position].setStartTime(time);
                nodes[position].setBeingHandled(true);
                total++;
            }
            time++;
        }while(total < 26);
        //}while(total < 6);

        int max = -1;
        for(int i=0;i<26;i++) {
            if(nodes[i] != null) {
                System.out.println("support.Node start " + nodes[i].getStartTime() + " duration " + nodes[i].getDuration());
                if(nodes[i].getDuration()+nodes[i].getStartTime() > max){
                    max = nodes[i].getDuration()+nodes[i].getStartTime();
                }
            }
        }

        System.out.println(max);
    }

    private static Node getNextAvailableNode(Node[] nodes, Vector<String> usedNodes){
        Vector<Integer> possibleNodes = new Vector<>();
        for(int i=0;i<26;i++){
            Node node = nodes[i];
            if(node == null)continue;

            boolean allDepenciesResolved = !usedNodes.contains(node.getName());
            for(String dependecy : node.getDependencies()){
                if(!usedNodes.contains(dependecy)){
                    allDepenciesResolved = false;
                    break;
                }
            }
            if(allDepenciesResolved && !node.isBeingHandled()){
                possibleNodes.add(i);
            }
        }
        Collections.sort(possibleNodes);
        return possibleNodes.size() > 0 ? nodes[possibleNodes.firstElement()] : null;
    }
}
