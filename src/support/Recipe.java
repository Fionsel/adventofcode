package support;

public class Recipe {
    private Recipe previous;
    private Recipe next;
    private int score;
    private int index;

    public Recipe(int score, int index){
        this.score = score;
        this.index = index;
        previous = this;
        next = this;
    }

    public int getScore() {
        return score;
    }

    public Recipe getPrevious() {
        return previous;
    }

    public void setPrevious(Recipe previous) {
        this.previous = previous;
    }

    public Recipe getNext() {
        return next;
    }

    public void setNext(Recipe next) {
        this.next = next;
    }

    public int getIndex() {
        return index;
    }
}
