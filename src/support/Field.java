package support;

public class Field{
    private int distance;
    private String item;
    private int number;

    public Field(int d, String i, int number){
        distance = d;
        item = i;
        this.number = number;
    }

    public int getDistance() {
        return distance;
    }

    public void setDistance(int d) {
        distance = d;
    }

    public String getItem() {
        return item;
    }

    public void setItem(String i) {
        item = i;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }
}
