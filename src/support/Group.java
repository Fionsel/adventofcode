package support;

import java.util.Vector;
import java.util.stream.Collectors;

public class Group implements Comparable<Group>{
    private int hitPoints;
    private int size;
    private Vector<String> immunities;
    private Vector<String> weaknessses;
    private int damage;
    private String damageType;
    private int initiative;
    private Group target;
    private boolean isImmunity;
    private boolean isTarget;
    private int counter;

    public Group(int hitPoints, int size, int damage, String damageType, int initiative, boolean isImmunity, int counter) {
        this.hitPoints = hitPoints;
        this.size = size;
        this.damage = damage;
        this.damageType = damageType;
        this.initiative = initiative;
        this.isImmunity = isImmunity;
        this.counter = counter;
        this.target = null;
        this.isTarget = false;
        immunities = new Vector<>();
        weaknessses = new Vector<>();
    }

    public int getSize() {
        return size;
    }

    public Vector<String> getImmunities() {
        return immunities;
    }

    public Vector<String> getWeaknessses() {
        return weaknessses;
    }

    public String getDamageType() {
        return damageType;
    }

    public int getInitiative() {
        return initiative;
    }

    public int getEffectivePower(){
        return size*damage;
    }

    public void determineTarget(Vector<Group> enemies){
        target = null;
        int damage = getEffectivePower();
        int previousDamage = -1;
        for (Group enemy : enemies.stream().filter(e -> !e.isTarget()).filter(e -> e.getSize() > 0).collect(Collectors.toList())){
            int totalDamage = damage * (enemy.getWeaknessses().contains(getDamageType()) ? 2 : 1);
            totalDamage = totalDamage * (enemy.getImmunities().contains(getDamageType()) ? 0 : 1);
            if(totalDamage > 0 && (totalDamage > previousDamage || (totalDamage == previousDamage && (enemy.getEffectivePower() > target.getEffectivePower() || (enemy.getEffectivePower() == target.getEffectivePower() && enemy.getInitiative() > target.getInitiative()))))){
                previousDamage = totalDamage;
                target = enemy;
            }
        }
        if(target != null)target.setTarget(true);
    }

    @Override
    public int compareTo(Group o) {
        if(this.getEffectivePower() < o.getEffectivePower())return 1;
        if(this.getEffectivePower() > o.getEffectivePower())return -1;
        if(this.getInitiative() < o.getInitiative())return 1;
        if(this.getInitiative() > o.getInitiative())return -1;
        return 0;
    }

    public int compareToAttack(Group o) {
        if(this.getInitiative() < o.getInitiative())return 1;
        if(this.getInitiative() > o.getInitiative())return -1;
        return 0;
    }

    public boolean isImmunity() {
        return isImmunity;
    }

    public void setTarget(boolean target) {
        isTarget = target;
    }

    public boolean isTarget(){
        return isTarget;
    }

    public void attackTarget(){
        if(target == null)return;

        int totalDamage = getEffectivePower() * (target.getWeaknessses().contains(getDamageType()) ? 2 : 1);
        totalDamage = totalDamage * (target.getImmunities().contains(getDamageType()) ? 0 : 1);
        target.receiveAttack(totalDamage);
    }

    public void receiveAttack(int totalDamage){
        int unitLost = totalDamage/hitPoints;
        size -= unitLost;
    }

    public int getCounter() {
        return counter;
    }

    public Group getTarget(){
        return target;
    }
}
