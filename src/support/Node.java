package support;

import java.util.Vector;

public class Node {
    private String name;
    private Vector<String> dependencies;
    private int duration;
    private int startTime;
    private boolean isBeingHandled = false;

    public Node(String name){
        this.name = name;
        dependencies = new Vector<>();
        duration = (name.charAt(0)-'A')+61;
        //duration = (name.charAt(0)-'A')+1;
    }

    public String getName() {
        return name;
    }

    public Vector<String> getDependencies() {
        return dependencies;
    }

    public int getDuration() {
        return duration;
    }

    public int getStartTime() {
        return startTime;
    }

    public void setStartTime(int startTime) {
        this.startTime = startTime;
    }

    public boolean isBeingHandled() {
        return isBeingHandled;
    }

    public void setBeingHandled(boolean beingHandled) {
        isBeingHandled = beingHandled;
    }
}
