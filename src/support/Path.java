package support;

import java.util.ArrayList;
import java.util.List;

public class Path {
    private List<Coordinate> path;

    public Path() {
        path = new ArrayList<>();
    }

    public List<Coordinate> getPath() {
        return path;
    }
}
