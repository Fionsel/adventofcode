package support;

public class LinkedListItem {
    private LinkedListItem previous;
    private LinkedListItem next;
    private int marble;

    public LinkedListItem(int marble){
        this.marble = marble;
        previous = null;
        next = null;
    }

    public LinkedListItem getPrevious() {
        return previous;
    }

    public void setPrevious(LinkedListItem previous) {
        this.previous = previous;
    }

    public LinkedListItem getNext() {
        return next;
    }

    public void setNext(LinkedListItem next) {
        this.next = next;
    }

    public int getMarble() {
        return marble;
    }
}
