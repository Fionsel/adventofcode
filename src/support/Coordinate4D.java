package support;

public class Coordinate4D {
    private int x;
    private int y;
    private int z;
    private int t;

    public Coordinate4D(int x, int y, int z, int t) {
        this.x = x;
        this.y = y;
        this.z = z;
        this.t = t;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public int getZ() {
        return z;
    }
    
    public int getT(){
        return t;
    }

    public int manhattanDistanceTo(Coordinate4D other){
        return Math.abs(other.getX()-this.getX())+Math.abs(other.getY()-this.getY())+Math.abs(other.getZ()-this.getZ())+Math.abs(other.getT()-this.getT());
    }
}
