package support;

public class Creature implements Comparable<Creature> {
    private int hitPoints;
    private int xcoordinate;
    private int ycoordinate;
    private boolean isElf;

    public Creature(int xcoordinate, int ycoordinate, boolean isElf) {
        this.xcoordinate = xcoordinate;
        this.ycoordinate = ycoordinate;
        this.isElf = isElf;
        hitPoints = 200;
    }

    public int getHitPoints() {
        return hitPoints;
    }

    public int getXcoordinate() {
        return xcoordinate;
    }

    public int getYcoordinate() {
        return ycoordinate;
    }

    public void move(Coordinate newCoordinate){
        xcoordinate = newCoordinate.getX();
        ycoordinate = newCoordinate.getY();
    }

    public void attack() {
        hitPoints -= isElf ? 3 : 34;
    }

    public boolean isElf() {
        return isElf;
    }

    public boolean isDead(){
        return hitPoints <= 0;
    }

    public int compareTo(Creature creature) {
        if (this.ycoordinate > creature.getYcoordinate()) return 1;
        if (this.ycoordinate < creature.getYcoordinate()) return -1;
        if (this.xcoordinate > creature.getXcoordinate()) return 1;
        return this.xcoordinate == creature.getXcoordinate() ? 0 : -1;
    }
}