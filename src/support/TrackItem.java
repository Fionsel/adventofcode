package support;

public class TrackItem {
    private char track;
    private Cart cart;

    public TrackItem(char track, Cart cart){
        this.track = track;
        this.cart = cart;
    }

    public boolean isOccupied(){
        return cart != null;
    }

    public void setCart(Cart cart){
        this.cart = cart;
    }

    public Cart getCart(){
        return cart;
    }

    public char getTrack() {
        return track;
    }
}
