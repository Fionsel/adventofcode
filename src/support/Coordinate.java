package support;

import java.util.ArrayList;
import java.util.List;

public class Coordinate implements Comparable<Coordinate> {
    private int x;
    private int y;
    private List<Coordinate> path;

    public Coordinate(int x, int y) {
        this.x = x;
        this.y = y;
        path = new ArrayList<>();
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public int compareTo(Coordinate coordinate) {
        if (this.y > coordinate.getY()) return 1;
        if (this.y < coordinate.getY()) return -1;
        if (this.x > coordinate.getX()) return 1;
        return this.x == coordinate.getX() ? 0 : -1;
    }

    public List<Coordinate> getPath() {
        return path;
    }
}
