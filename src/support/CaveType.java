package support;

public class CaveType implements Comparable<CaveType>{
    private int duration;
    private int type;
    private int equipment;
    private int x;
    private int y;

    public CaveType(int duration, int type, int equipment, int x, int y) {
        this.duration = duration;
        this.type = type;
        this.equipment = equipment;
        this.x = x;
        this.y = y;
    }

    public int getEquipment() {
        return equipment;
    }

    public int getType() {
        return type;
    }

    public int getDuration() {
        return duration;
    }

    @Override
    public int compareTo(CaveType other) {
        return this.duration < other.getDuration() ? -1 : this.duration == other.getDuration() ? 0 : 1;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }
}
