package support;

public class Cart {
    private int lastChosenDirection = -1;
    private int currentDirectionX;
    private int currentDirectionY;
    private boolean hasMoved;

    public Cart(int currentDirectionY, int currentDirectionX){
        this.currentDirectionX = currentDirectionX;
        this.currentDirectionY = currentDirectionY;
        hasMoved = false;
    }

    private int getNextDirection(){
        lastChosenDirection++;
        lastChosenDirection%=3;
        return lastChosenDirection;
    }


    public int getCurrentDirectionX() {
        return currentDirectionX;
    }

    public int getCurrentDirectionY() {
        return currentDirectionY;
    }

    public void adjustDirection(char trackItem){
        if(trackItem == '\\'){
            doTurn(currentDirectionY);
        }
        else if(trackItem == '/'){
            doTurn(currentDirectionX);
        }
        else if(trackItem == '+'){
            int nextDirection = getNextDirection();
            if(nextDirection == 0)
            {
                leftTurn();
            }
            else if(nextDirection == 2)
            {
                rightTurn();
            }
        }
    }

    private void doTurn(int axis){
        if(axis != 0)
            leftTurn();
        else
            rightTurn();
    }

    private void leftTurn(){
        int tempX = currentDirectionX;
        currentDirectionX = currentDirectionX == 0 ? currentDirectionY : 0;
        currentDirectionY = currentDirectionY == 0 ? -tempX : 0;
    }

    private void rightTurn(){
        int tempX = currentDirectionX;
        currentDirectionX = currentDirectionX == 0 ? -currentDirectionY : 0;
        currentDirectionY = currentDirectionY == 0 ? tempX : 0;
    }

    public boolean hasMoved() {
        return hasMoved;
    }

    public void setHasMoved(boolean hasMoved) {
        this.hasMoved = hasMoved;
    }
}
