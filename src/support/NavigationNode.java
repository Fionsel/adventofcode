package support;

import java.util.ArrayList;
import java.util.List;

public class NavigationNode {
    private int numberOfChildNodes;
    private int metadataSize;
    private List<NavigationNode> childNodes;
    private List<Integer> metadata;

    public NavigationNode(int numberOfChildNodes, int metadataSize) {
        this.numberOfChildNodes = numberOfChildNodes;
        this.metadataSize = metadataSize;
        childNodes = new ArrayList<>();
        metadata = new ArrayList<>();
    }

    public int getNumberOfChildNodes() {
        return numberOfChildNodes;
    }

    public int getMetadataSize() {
        return metadataSize;
    }

    public List<NavigationNode> getChildNodes() {
        return childNodes;
    }

    public List<Integer> getMetadata() {
        return metadata;
    }

    public int getMetadataSum(){
        int sum = metadata.stream().mapToInt(Integer::intValue).sum();
        for(NavigationNode child : childNodes)
            sum += child.getMetadataSum();
        return sum;
    }

    public int getReferenceValue(){
        if(numberOfChildNodes == 0) return getMetadataSum();

        int sum = 0;
        for(Integer reference : metadata){
            if(reference > numberOfChildNodes) continue;
            if(reference == 0)continue;

            sum += childNodes.get(reference-1).getReferenceValue();
        }
        return sum;
    }
}
