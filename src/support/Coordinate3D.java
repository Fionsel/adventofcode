package support;

public class Coordinate3D {
    private long x;
    private long y;
    private long z;
    private long radius;

    public Coordinate3D(long x, long y, long z, long radius) {
        this.x = x;
        this.y = y;
        this.z = z;
        this.radius = radius;
    }

    public long getX() {
        return x;
    }

    public long getY() {
        return y;
    }

    public long getRadius() {
        return radius;
    }

    public long manhattanDistanceTo(Coordinate3D other){
        return Math.abs(other.getX()-this.getX())+Math.abs(other.getY()-this.getY())+Math.abs(other.getZ()-this.getZ());
    }

    public long getZ() {
        return z;
    }
}
