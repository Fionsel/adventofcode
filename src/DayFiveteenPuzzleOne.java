import support.Coordinate;
import support.Creature;
import support.Path;

import java.util.*;
import java.util.stream.Collectors;

public class DayFiveteenPuzzleOne {
    /*static String[] input = new String[]{
            "#######" ,
            "#.G...#" ,
            "#...EG#" ,
            "#.#.#G#" ,
            "#..G#E#" ,
            "#.....#" ,
            "#######"
    };*/
    
    /*static String[] input = new String[]{
            "#######" ,
            "#G..#E#" ,
            "#E#E.E#" ,
            "#G.##.#" ,
            "#...#E#" ,
            "#...E.#" ,
            "#######"
    };*/
    
    /*static String[] input = new String[]{
            "#######" ,
            "#E..EG#" ,
            "#.#G.E#" ,
            "#E.##E#" ,
            "#G..#.#" ,
            "#..E#.#" ,
            "#######"
    };*/
    
    /*static String[] input = new String[]{
            "#######" ,
            "#E.G#.#" ,
            "#.#G..#" ,
            "#G.#.G#" ,
            "#G..#.#" ,
            "#...E.#" ,
            "#######"
    };*/
    
    /*static String[] input = new String[]{
            "#######" ,
            "#.E...#" ,
            "#.#..G#" ,
            "#.###.#" ,
            "#E#G#G#" ,
            "#...#G#" ,
            "#######"
    };*/

    /*static String[] input = new String[]{
            "#########" ,
            "#G......#" ,
            "#.E.#...#" ,
            "#..##..G#" ,
            "#...##..#" ,
            "#...#...#" ,
            "#.G...G.#" ,
            "#.....G.#" ,
            "#########"
    };*/
    
    /*static String[] input = new String[]{
            "#####",
            "#GG##",
            "#.###",
            "#..E#",
            "#.#G#",
            "#.E##",
            "#####"
    };*/    
    
    static String[] input = new String[]{
            "################################",
            "#G..#####G.#####################",
            "##.#####...#####################",
            "##.#######..####################",
            "#...#####.#.#.G...#.##...###...#",
            "##.######....#...G..#...####..##",
            "##....#....G.........E..####.###",
            "#####..#...G........G...##....##",
            "######.....G............#.....##",
            "######....G.............#....###",
            "#####..##.......E..##.#......###",
            "########.##...........##.....###",
            "####G.G.......#####..E###...####",
            "##.......G...#######..#####..###",
            "#........#..#########.###...####",
            "#.G..GG.###.#########.##...#####",
            "#...........#########......#####",
            "##..........#########..#.#######",
            "###G.G......#########....#######",
            "##...#.......#######.G...#######",
            "##.......G....#####.E...#.######",
            "###......E..G.E......E.....#####",
            "##.#................E.#...######",
            "#....#...................#######",
            "#....#E........E.##.#....#######",
            "#......###.#..#..##.#....#..####",
            "#...########..#..####....#..####",
            "#...########.#########......####",
            "#...########.###################",
            "############.###################",
            "#########....###################",
            "################################"
    };

    static String[][] grid;
    static Vector<Creature> creatures = new Vector<>();

    public static void main(String[] args){
        grid = new String[input.length][];
        for(int i=0;i<input.length;i++){
            grid[i] = input[i].split("");
            for(int j=0;j<grid[i].length;j++){
                if(grid[i][j].equals("G")){
                    creatures.add(new Creature(j,i, false));
                } else if(grid[i][j].equals("E")){
                    creatures.add(new Creature(j,i,true));
                }
            }
        }
        int fullRounds = 0;
        boolean oneSideDead = false;
        do{
            List<Creature> aliveCreatures = creatures.stream().filter(c -> !c.isDead()).collect(Collectors.toList());
            Collections.sort(aliveCreatures);
            int i;
            for(i=0;i<aliveCreatures.size();i++){
                if(oneSideDead)break;

                Creature creature = aliveCreatures.get(i);
                if(creature.isDead())continue;

                boolean hasAction = performMove(creature);
                if(!hasAction) continue;

                performAttackRound(creature);
                if(creatures.stream().filter(Creature::isElf).allMatch(Creature::isDead)) {
                    oneSideDead = true;
                }
                if(creatures.stream().filter(c -> !c.isElf()).allMatch(Creature::isDead)) {
                    oneSideDead = true;
                }
            }
            if(i == aliveCreatures.size()) {
                fullRounds++;
                //System.out.println(fullRounds);
                //printGrid(grid);
                //printHealth();
            }

        }while(!oneSideDead);
        //printGrid(grid);
        //printHealth();
        int totalHealth = creatures.stream().filter(c -> !c.isDead()).map(Creature::getHitPoints).mapToInt(Integer::intValue).sum();
        System.out.println(fullRounds*totalHealth);
        //System.out.println(creatures.stream().filter(Creature::isElf).anyMatch(Creature::isDead));
    }

    private static void printGrid(String[][] grid){
        for(String[] line : grid) {
            for(String field : line)
                System.out.print(field);
            System.out.println();
        }
    }

    private static void printHealth(){
        List<Creature> aliveCreatures = creatures.stream().filter(c -> !c.isDead()).sorted(Creature::compareTo).collect(Collectors.toList());
        aliveCreatures.forEach(c -> System.out.println((c.isElf() ? "E ": "G ") + c.getHitPoints()));
    }
    
    private static boolean performMove(Creature creature){
        List<Creature> directEnemies = getDirectEnemies(creature);
        if(directEnemies.size() > 0) return true;

        List<Creature> enemies = creatures.stream().filter(c -> c.isElf()!=creature.isElf()&&!c.isDead()).collect(Collectors.toList());
        List<Coordinate> enemyCoordinates = new ArrayList<>();
        for(Creature enemy : enemies){
            if(grid[enemy.getYcoordinate()-1][enemy.getXcoordinate()].equals("."))
                enemyCoordinates.add(new Coordinate(enemy.getXcoordinate(),enemy.getYcoordinate()-1));
            if(grid[enemy.getYcoordinate()+1][enemy.getXcoordinate()].equals("."))
                enemyCoordinates.add(new Coordinate(enemy.getXcoordinate(),enemy.getYcoordinate()+1));
            if(grid[enemy.getYcoordinate()][enemy.getXcoordinate()-1].equals("."))
                enemyCoordinates.add(new Coordinate(enemy.getXcoordinate()-1,enemy.getYcoordinate()));
            if(grid[enemy.getYcoordinate()][enemy.getXcoordinate()+1].equals("."))
                enemyCoordinates.add(new Coordinate(enemy.getXcoordinate()+1,enemy.getYcoordinate()));
        }
        Path[][] distances = determineShortestPaths(creature);
        List<Coordinate> reachableEnemies = enemyCoordinates.stream().filter(e -> distances[e.getY()][e.getX()] != null).collect(Collectors.toList());
        if(reachableEnemies.size()==0) return false;

        reachableEnemies.forEach(e -> e.getPath().addAll(distances[e.getY()][e.getX()].getPath()));

        int min = reachableEnemies.stream().map(e -> e.getPath().size()).min(Integer::compareTo).get();
        Coordinate closestEnemy = reachableEnemies.stream().filter(p -> p.getPath().size() == min).sorted(Coordinate::compareTo).findFirst().get();
        Coordinate coordinateToMoveTo = closestEnemy.getPath().size() > 1 ? closestEnemy.getPath().get(1) : closestEnemy;
        move(creature, coordinateToMoveTo);
        return true;
    }

    private static void move(Creature creature, Coordinate next){
        grid[creature.getYcoordinate()][creature.getXcoordinate()]=".";
        creature.move(next);
        grid[creature.getYcoordinate()][creature.getXcoordinate()]=creature.isElf() ? "E" : "G";
    }

    private static Path[][] determineShortestPaths(Creature creature){
        Path[][] distances = new Path[grid.length][grid[0].length];
        distances[creature.getYcoordinate()][creature.getXcoordinate()] = new Path();
        iteratePath(distances, creature.getYcoordinate(), creature.getXcoordinate());
        return distances;
    }

    private static void iteratePath(Path[][] distances, int i, int j){
        handleField(distances, i-1, j, i, j);
        handleField(distances, i, j-1, i, j);
        handleField(distances, i, j+1, i, j);
        handleField(distances, i+1, j, i, j);
    }

    private static void handleField(Path[][] distances, int i, int j, int origi, int origj){
        if(!grid[i][j].equals(".")) return;

        if(distances[i][j] == null || distances[origi][origj].getPath().size()+1 < distances[i][j].getPath().size()) {
            updateField(distances, i, j, origi, origj);
        }
    }

    private static void updateField(Path[][] distances, int i, int j, int origi, int origj){
        distances[i][j] = new Path();
        distances[i][j].getPath().addAll(distances[origi][origj].getPath());
        distances[i][j].getPath().add(new Coordinate(origj, origi));
        iteratePath(distances, i, j);
    }

    private static void performAttackRound(Creature creature){
        List<Creature> enemies = getDirectEnemies(creature);
        if(enemies.size() == 0) return;

        int lowestHealth = enemies.stream().map(Creature::getHitPoints).min(Integer::compareTo).get();
        Creature target = enemies.stream().filter(c -> c.getHitPoints()==lowestHealth).sorted(Creature::compareTo).findFirst().get();
        target.attack();
        if(target.isDead())
            grid[target.getYcoordinate()][target.getXcoordinate()]=".";
    }

    private static Creature getCreatureForCoordinates(int x, int y){
        return creatures.stream().filter(c -> c.getXcoordinate()==x && c.getYcoordinate()==y && !c.isDead()).findFirst().get();
    }

    private static List<Creature> getDirectEnemies(Creature creature){
        List<Creature> enemies = new ArrayList<>();
        if(grid[creature.getYcoordinate()-1][creature.getXcoordinate()].equals(creature.isElf()?"G":"E"))
            enemies.add(getCreatureForCoordinates(creature.getXcoordinate(),creature.getYcoordinate()-1));
        if(grid[creature.getYcoordinate()+1][creature.getXcoordinate()].equals(creature.isElf()?"G":"E"))
            enemies.add(getCreatureForCoordinates(creature.getXcoordinate(),creature.getYcoordinate()+1));
        if(grid[creature.getYcoordinate()][creature.getXcoordinate()-1].equals(creature.isElf()?"G":"E"))
            enemies.add(getCreatureForCoordinates(creature.getXcoordinate()-1,creature.getYcoordinate()));
        if(grid[creature.getYcoordinate()][creature.getXcoordinate()+1].equals(creature.isElf()?"G":"E"))
            enemies.add(getCreatureForCoordinates(creature.getXcoordinate()+1,creature.getYcoordinate()));
        return enemies;
    }
}
