import support.Cart;
import support.TrackItem;

public class DayThirteenPuzzleOne {
    
    static String[] input = new String[]{
                    "                             /-----------------------------------------------------------------\\  /-----------------\\                                 ",
                    "                             |                 /-----------------\\                             |  |                 |                                 ",
                    "                             |                 | /---------------+------------\\           /----+--+\\          /-----+----------------------\\          ",
                    "                             |                 | |               |            |           |    |  ||          |     |                      |          ",
                    "            /----------------+-----------------+-+<--------------+--\\         |           |    |  ||          |     |                      |          ",
                    "            |                |                 | |               |  |         |           |    |  ||          |     |                      |          ",
                    "            |                |                 | |        /------+--+---------+-----------+----+--++----------+----\\|                      |          ",
                    "            |                |                 | |        |      |  |  /------+-----------+----+--++---\\    /-+----++--------------------\\ |          ",
                    "   /--------+-\\              | /---------------+-+--------+------+--+--+------+-----------+----+--++---+----+-+----++---------\\          | |          ",
                    "   |        | |              | |              /+-+--------+------+--+--+------+---------\\ |    |  ||   |    | |    ||         |          | |          ",
                    "   |   /----+-+----------\\   | |              || |        |      |  |  |      |/--------+-+----+--++---+----+-+----++------\\  |          | |          ",
                    "   |   |    | |          |   | |         /----++-+--------+------+--+\\ |      ||        | |    |  ||   |    | |    ||      |  |          | |          ",
                    "   |   |    | |          |   | |         |    |\\-+--------+------/  || |/-----++------\\ | |    |  ||   |    | |    ||/-----+--+\\         | |          ",
                    "   |   |    | |          |   | |    /----+----+--+--------+\\        || ||/----++------+-+-+----+--++---+----+-+----+++-----+--++---------+-+\\         ",
                    "   |   |  /-+-+----------+---+-+----+----+----+--+--------++--------++-+++----++------+-+-+----+--++---+-\\  | |    |||     |  ||         | ||         ",
                    "   |   |  | | |          |   | |    |    |    |  |        ||        || |||    ||      | | |    |/-++---+-+--+-+----+++-----+\\ ||         | ||         ",
                    "   |   |  | | |          |   | |    |    |    |  |        ||        || |||    ||      | | |   /++-++---+-+--+-+----+++-----++-++----\\    | ||         ",
                    "   |   |  | | |/---------+---+-+----+\\   |    |  |        ||        || |||    ||      | | |   ||| ||   | |  | |    |||     || ||    |    | ||         ",
                    "   |   |  | | ||         |   | |    ||   |    |  |        ||        ||/+++----++------+-+-+---+++-++---+-+--+-+----+++-----++-++----+--\\ | ||         ",
                    "   |   |  | | ||         |   | |  /-++---+----+--+--------++-----<--++++++----++------+-+-+---+++-++---+-+--+-+----+++-----++\\||    |  | | ||         ",
                    "   |   |  | | ||         |   | |  | ||   |    |  |        ||       /++++++----++------+-+-+---+++-++---+-+-<+-+----+++----\\|||||    |  | | ||         ",
                    "   |   |  | | ||         |   | |  | ||   |    |  |        ||       |||||||    ||      | | |   ||| ||   | |  | |    |||    ||||||    |  | | ||         ",
                    "   |   |  | | ||         |   | |  |/++---+----+--+--------++-------+++++++----++------+-+-+---+++-++---+-+--+-+----+++----++++++----+--+-+-++\\        ",
                    "   |   |  | | ||     /---+---+-+--++++---+----+\\ |        ||       |||||||    ||      | | |   ||| ||   | |  | |    |||    ||||||    |  | | |||        ",
                    "   |   |  | | ||     |   |   | |  ||||   |    || |        ||   /---+++++++----++------+-+-+---+++-++---+<+--+-+----+++----++++++\\   |  | | |||        ",
                    "   |   |  | | ||     |   |   | |  ||||/--+----++-+--------++---+---+++++++----++------+-+-+---+++-++---+-+--+-+-\\  |||    |||||||   |  | | |||        ",
                    "   |   |  | | ||     |   |   | |  |||||  \\----++-+--------++---+---++/||||    ||      | | |   ||| ||   | |  | | |  |||    |||||||   |  | | |||        ",
                    "   \\---+--+-+-/|  /--+---+---+-+--+++++-------++-+--------++---+---++-++++----++------+-+-+---+++-++\\  | |  | | |  |||    ||||||| /-+--+-+-+++\\       ",
                    "       |  | | /+--+--+---+---+-+--+++++-------++-+--------++---+---++-++++----++------+-+-+---+++-+++--+-+--+-+-+--+++-\\  ||||||| | |  | | ||||       ",
                    "       |  | | ||  |  |   |   | |  |||||   /---++-+--------++---+---++-++++----++------+-+-+---+++-+++--+-+-\\| | |  |||/+--+++++++-+-+--+-+-++++-\\     ",
                    "       |  | | ||  |  |   |   | |  |||||   |   || |        ||   |   || ||||    ||   /--+-+-+-\\ ||| |||  | | || | |  |||||  ||||||| | |  | | |||| |     ",
                    "       |  | | ||  |  |   |   | |  |||||   |   || |        \\+---+---++-++++----++---+--+-+-+-+-+++-+++--+-+-++-+-+--/||||  ||||||| | |  | | |||| |     ",
                    "       |  | \\-++--+--+---+---+-+--+++++---+---++-+---------+---+---+/ ||||    ||   |  | | | | ||| |||  | | || | |   ||||  ||||||| | |  | | |||| |     ",
                    "       |  |   ||  |  |   |   | |  |||||   |   || |         |   |   |  ||||    ||   |  | | | | ||| |||  | | || | |   ||||  ||||||| | |  | | |||| |     ",
                    "     /-+--+---++--+-\\|   |   | |  |||||   |   || |         |   |   |  ||||  /-++---+--+-+-+-+-+++-+++--+-+-++-+-+---++++--+++++++-+-+-\\| | |||| |     ",
                    "     | |  |   ||  | ||   |   | |  |||||   |   || |       /-+---+---+--++++--+-++---+--+-+-+-+-+++-+++--+\\| || | |   ||||  ||||||| | | || | |||| |     ",
                    "     | |  |   ||  | ||   |   | |  |||||   |   || |       | |   |   |  ||||  | ||   |  | | | | ||| |||  ||| || \\-+---++++--+++++++-+-+-++-+-/||| |     ",
                    "     | |  |   ||  | ||   |   | |  |||||   |   || |       | |   |   | /++++--+-++---+--+-+-+-+-+++-+++--+++\\||   | /-++++--+++++++-+-+-++-+--+++-+-<-\\ ",
                    "     | |  |   ||  | ||   |   | |  ||\\++---+---++-+-------+-/   |   | |||||  | ||   |  | | | | ||| |||  ||||||   | | ||||  ||||||| | | || |  ||| |   | ",
                    "     |/+--+---++--+-++---+---+-+--++-++---+---++-+-------+-----+---+-+++++--+-++---+--+-+-+-+-+++-+++--++++++\\  | | ||||  ||||||| | | || |  ||| |   | ",
                    "     |||  |   ||  | ||   |   | |  || || /-+---++-+-------+-----+---+-+++++--+-++---+--+-+-+-+-+++-+++--+++++++\\ | | ||||  ||||||| | | || |  ||| |   | ",
                    "     |||  |   ||  | ||   |   \\-+--++-++-+-+---++-+-------+-----+---+-+++++--+-++---+--+-+-+-+-+/| |||  |||||||| | | ||||  ||||||| | | || |  ||| |   | ",
                    "     |||  |   ||  | ||   |     |  || || | |   || |       |     \\---+-+++++--+-++---+--+-+-+-+-+-+-+++--++++++++-+-+-++++--++++++/ | | || |  ||| |   | ",
                    "     |||  |   ||  | ||   |     |  || || | |   || |       |       /-+-+++++\\ | ||   |  | | | | | | \\++--++++++++-+-+-/|||  ||||||  | | || |  ||| |   | ",
                    "     |||  |   ||  | ||   |     |  || || | |   || |       |       | | |||||| | ||   |  | | |/+-+-+--++--++++++++-+-+--+++--++++++--+-+-++-+--+++-+---+\\",
                    "     |||  |   ||  | ||   |     |  || || | |/--++-+-------+-------+-+-++++++-+-++---+--+-+-+++-+\\|  ||  |||||||| | |  |||  ||||||  | | || |  ||| |   ||",
                    "     |||  |   ||  | ||   |     |  || || | ||  || |       |       | | |||||| | ||   |  | | ||| |||  ||  |||||||| | |  |||  ||||||  | | || |  ||| |   ||",
                    "     |||  \\---++--+-++---+->---+--++-++-+-++--++-+-------+-------+-+-++++++-+-++---+--+-+-+++-+++--++--++/||||| | |  |\\+--++++++--+-+-++-+--+++-/   ||",
                    "     |||      ||  | ||   |     |  || || | ||  || |       |    /--+-+-++++++-+-++---+--+\\| ||| |||  ||  || ||||| | |  | |  ||||||  | | || |  |||     ||",
                    "     |||      ||  | ||   | /---+--++-++-+-++--++-+--\\    |    |  | | |||||| | ||   |  ||| ||| |||  ||  || ||||| | |  | |  ||||||  | | || |  |||     ||",
                    "     |||      ||  | ||   | |   |  || || | ||  || |  |    |    |  | | |||||| | ||   |  ||| ||| |||  ||  || ||||| | |  | |  ||||||  | | || |  |||     ||",
                    "     |||      ||  | ||   | |   |  || || | ||  || |  |    |    |  | | |||||| | ||   |/-+++-+++-+++-\\||  || ||||| | |  | |  ||||||  | | || |  |||     ||",
                    "     |||      ||  | ||   | |   |  || || | ||  \\+-+--+----+----+--+-+-++++++-+-++---++-++/ ||| ||| |||  || ||||| | |  | |  ||||||  | | || |  |||     ||",
                    "     |||      ||  | ||   | |   |  || || | ||/--+-+--+--\\ |    |  | | |||||| | ||   || ||  ||| ||| |||  || ||||| | |  | |  ||||||  | | || |  |||     ||",
                    "     |||      ||  | ||   | |   |  || || | |||  | |  |  | |    |  | | ||\\+++-+-++---++-++--+++-+++-+++--/| ||||| | |  |/+--++++++\\ | | || |  |||     ||",
                    "     |||      ||  | ||   | |   |  || ||/+-+++--+-+--+--+-+----+--+-+-++-+++-+-++---++-++--+++-+++-+++---+-+++++\\| |/-+++--+++++++-+-+-++-+--+++\\    |v",
                    " /---+++------++--+-++---+-+---+--++\\|||| |||  | |  |  | |    |  | | || ||| | |\\---++-++--+++-+++-+++---+-+++++++-++-+++--+/||||| | | || |  ||||    ||",
                    " |   |||/-----++--+-++---+-+-\\ |  ||||||| |||  | |  |  |/+----+--+-+-++-+++-+-+----++-++--+++-+++-+++---+-+++++++-++-+++-\\| ||||| | |/++-+--++++-\\  ||",
                    " |   ||||     ||  | ||   | | | |  ||||||| |||  | |  |  |||    |  | | || |||/+-+----++-++--+++-+++-+++---+-+++++++-++\\||| || ||||| | |||| |  |||| |  ||",
                    " |   ||||     ||  | ||   | | | |/-+++++++-+++--+-+--+--+++----+--+-+-++-+++++-+----++-++--+++\\||| |||   | ||||||| |||||| || ||||| | |||| |  |||| |  ||",
                    " |   ||||     ||  | ||   | | | || ||||||| |||  | |  |  |||    |  | | || ||||| |    || ||  \\++++++-+/|   | ||||||| |||||| || ||||| | |||| |  |||| |  ||",
                    " |   ||||     ||  | ||   | | | || ||||||| |||  | |  |  |||    |  | \\-++-+++++-+----++-++---++++++-+-+---+-+++++++-++++++-+/ ||||| | |||| |  |||| |  ||",
                    " |   ||||     ||  | ||   | | | || ||||||| |||  | |  |  |||    \\--+---++-+++++-+----++-+/   |||||| | |   | ||||||| |||||| |  |||^| | |||| |  |||| |  ||",
                    " |   ||||     ||  | ||   | | | || ||||||| |||  | | /+--+++-------+---++-+++++-+---\\|| |    |||||| | |   | ||||||| \\+++++-+--+++++-+-++++-+--++++-+--/|",
                    " |   ||||     ||  | ||   | | | || ||||||| |||  | | ||  |||       |   || ||||| |   ||| |    |||||| | |   | ||\\++++--+++++-+--+++++-+-++++-/  |||| |   |",
                    " |   ||||     ||  | ||   | | |/++-+++++++-+++--+-+-++--+++-------+---++-+++++-+---+++-+----++++++-+-+->-+-++-++++--+++++-+--+++++-+-++++--\\ |||| |   |",
                    " |   ||||     ||  | ||   | | |^|| ||||||| |||  | | ||  |||       |   || ||||| |   ||| |    |||||| | |   | || ||||  ||||| |  ||||| | ||||  | |||| |   |",
                    " |   ||||     ||  | ||   | | |||| ||||||| |||  | | ||  |||       |   || ||||| |   ||| |    |||||| | |   | || ||||  |||\\+-+--++++/ | ||||  | |||| |   |",
                    " |   ||||     ||  | ||   | | |||| ||||||| |||  | | ||  |||      /+---++-+++++-+---+++-+----++++++-+-+---+-++-++++--+++-+-+--++++--+-++++-\\| |||| |   |",
                    " |   ||||     ||  | ||   | | |||| ||||||| |||  | | ||  |||      ||   || ||||| |   ||| |    |||||| | |   | || ||||  ||| | |  ||||  | |\\++-++-++++-/   |",
                    " |   ||||/----++--+-++---+-+-++++-+++++++-+++--+-+-++--+++------++\\  || ||||| |   ||| |    |||||| | |   | || ||||  \\++-+-+--++++--+-+-++-++-+++/     |",
                    " |   ||\\++----++--+-++---/ | |||| |||||||/+++--+-+-++--+++------+++--++-+++++-+---+++-+----++++++-+-+---+-++-++++---++-+-+--++++--+-+-++-++-+++\\     |",
                    " |/--++-++----++--+-++-----+-++++-+++++++++++--+-+-++--+++------+++--++-+++++-+---+++-+----++++++-+-+-\\ | || ||||   || | |  ||||  | | || || ||||     |",
                    " ||  |\\-++----++--+-++-----+-++++-+++++++++++--+-+-++--+++------+++--++-+++++-+---+++-+----++++++-+-+-+-+-++-/|||   || | |  ||||  | | ||/++-++++----\\|",
                    " || /+--++----++-\\| ||     | ||\\+-+++++++++++--+-+-++--+++------+++--++-+++++-+---+++-+----++++++-+-+-+-+-++--+++---++-+-+--++/| /+-+-+++++\\||||    ||",
                    " || ||  ||    || || ||     | || | |||||||||||  | | ||  |||      |||  || ||||| |   ||| |    |||||| | | | | ||  |||   || | |  || | || | ||||||||||    ||",
                    " || ||  ||    || || ||    /+-++-+-+++++++++++--+-+-++--+++------+++-\\|| ||||| |   ||| |    |||||| |/+-+-+-++--+++---++-+-+--++-+-++-+-++++++++++-\\  ||",
                    " || ||  ||    ||/++-++----++-++-+-+++++++++++--+-+-++--+++------+++-+++-+++++-+---+++-+\\   |||||| ||| | | ||  |||   || | |  || | || | |||||||||| |  ||",
                    " || ||  ||    ||||| ||    || || | ||||||||\\++--+-+-++--+++------+++-+++-+++++-+---+++-++---++++++-+++-+-+-+/  |||   || | |  || | || | |||||||||| |  ||",
                    " || ||  ||    ||||\\-++----++-++-+-++++++++-++--+-+-++--+++------+++-+++-+++++-+---+++-++---++++++-++/ | | |   |||   |\\-+-+--++-/ || | |||||||||| |  ||",
                    " || ||  ||    ||||  ||    || || | |||||||| ||  | | ||  |||      ||| ||| \\++++-+---+++-/|   |||||| ||  | | |   |||   |  | |  ||   || | |||||||||| |  ||",
                    " || ||  ||    ||||  ||    || || | |||||||| ||  | \\-++--+++------+++-+++--++++-/   |||  |/--++++++\\||  | | | /-+++---+--+-+--++---++-+-++++++++++-+-\\||",
                    " || ||  ||    ||||  ||    || || | |||||||| \\+--+---++--+++------+++-+++--++++-----+++--++--++++/||||  | | | | |||   |  | |  ||   || | |||||||||| | |||",
                    " || ||  ||    ||||  ||    || || | ||||||||  |  |   ||  |||      ||| |||  ||||     |||  ||  |||| ||||  | | | | |||   |  | |  ||   || | |||||||||| | |||",
                    " || ||  ||    |||| /++----++-++-+-++++++++--+--+---++--+++------+++-+++--++++-----+++--++\\ |||| ||||  | | | | |||   |  | |  ||   || | |||||||||| | |||",
                    " || ||  ||    |||| |||    || || | ||||||||  |  |   ||  ||| /----+++-+++--++++-----+++--+++-++++-++++--+-+-+-+-+++---+--+-+--++---++-+-++++++++++-+\\|||",
                    " || ||  ||    |||| ||\\----++-++-+-++++++++--+--/   ||  ||| |    |\\+-+++--+/\\+-----+++--+++-++++-++++--+-+-+-+-+++---/  | |  ||   || | |||||||||| |||||",
                    " || ||  ||    |||| ||     || || | |\\++++++--+------++--+++-+----+-+-+++--+--+-----+++--+++-++++-++++--+-+-+-+-+++------+-+--++---++-+-+++++++/|| |||||",
                    " || ||  ||    |||| ||     || || | | ||\\+++--+------++--+++-+----+-+-+++--+--+-----+++--+++-++++-++++--+-+-+-+-++/      | |  ||   || | ||||||| || |||||",
                    " || ||  ||    |||| ||     || || | | || |||  |      ||  ||| |    | | |||  |  |     |||  ||| |||| ||||  | | | | ||       | |  ||   || | ||\\++++-++-+++/|",
                    " || ||  ||    |||| ||/----++-++-+-+-++-+++--+------++--+++-+----+-+-+++--+--+--\\  |||  ||| |||| ||||  | | | | ||      /+-+--++--\\|| | || |||| || ||| |",
                    "/++-++--++----++++-+++----++-++-+-+-++-+++-\\|      ||  ||| |    | | ||\\--+--+--+--+++--+++-++++-++++--+-+-+-+-++------++-+--++--+++-+-+/ |||| || ||| |",
                    "||| ||  ||  /-++++-+++----++-++-+-+-++-+++-++\\     ||  ||| |    | | ||   |  |  |  |||  ||| |||| ||||  | | | | ||      || |  ||  ||| |/+--++++-++-+++\\|",
                    "||\\-++--++--+-++++-+++----++-++-+-+-++-+++-+++-----++--+++-+----+-+-++---+--+--+--+++--+++-++++-++++--/ | | | ||      || |  ||  ||| |||  |||| || |||||",
                    "||  ||  ||  | |||| |||    || || | | || ||| |||     ||  ||| |    \\-+-++---+--+--+--+++--+++-++++-++++----+-+-+-++------++-+--++--+++-+++--/||| || |||||",
                    "||  |\\--++--+-++++-+/|    || || | | || ||| |||     ||  ||| |      | ||   \\--+--+--+++--+++-++++-++++----+-+-+-++------++-+--++--+++-+++---++/ || |||||",
                    "||  |   ||  | |||| | |    || || | | || ||| |||     ||  ||| |      | ||  /---+--+--+++--+++-++++-++++----+-+-+-++------++-+--++--+++-+++-\\ ||  || |||||",
                    "||  |   ||  | |||| | |    || || | | || ||| |||     ||  ||| |      | ||  |   |  |  ||\\--+++-++++-++/|    | | | ||      || |  ||  ||| ||| | ||  || |||||",
                    "|\\--+---++--+-++++-+-+----++-++-+-+-/| ||| |||     ||  ||| |      | ||  |   |  |  ||   ||| |||| || |    | | | ||      || |  ||  ||| ||| | ||  || |||||",
                    "|   |  /++--+-++++-+-+----++-++-+\\|  | ||| |||     ||  ||| |      |/++--+---+--+--++---+++\\|||| || |    | | | ||      || |  ||  ||| ||| | ||  || |||||",
                    "|   |  |||  | |||| | |    || || |||  | ||| |||     ||  ||| |      ||||  |   |  |  ||   |||||||| || |    | | | ||      || |  ||  ||| ||| | ||  || |||||",
                    "|   |  |||  | |||| | |    || || |||  | ||| |||     ||  ||| |      ||||  |   |  |  ||   |||||||| || |    | | | ||      || |  ||  ||| ||| | ||  || |||||",
                    "|   |  |||  | |||| | |    || || |||  | ||| |||     ||  ^|| |      ||||  |   |  | /++---++++++++-++-+----+-+-+-++--\\   || |  ||  ||| ||| | ||  || |||||",
                    "|   \\--+++--+-+++/ | |    || || |||  | ||| |||     ||  ||| |      ||||  |   |  | |||   |||||||| || |    | | | ||  |   || |  ||  ||| ||| | ||  || |||||",
                    "|      |||  | |||  | |    ||/++-+++--+-+++-+++-----++--+++-+------++++--+---+--+-+++---++++++++-++-+----+-+-+-++--+---++-+--++--+++\\||| | ||  || |||||",
                    "|      |||  | |||/-+-+----+++++-+++--+-+++-+++----\\||  ||| |      ||||  |   |  | |||   ||||\\+++-++-+----+-+-+-++--+---++-+--++--+++++++-+-++--++-++++/",
                    "|      |||  | |||| | |    |||||/+++--+-+++-+++----+++--+++-+------++++--+---+--+-+++---++++-+++-++-+----+-+-+-++--+---++\\|  ||  ||||||| | ||  || |||| ",
                    "|      |||  | |||| | |    \\++++++++--+-+++-+++----+++--+++-+------++/|  |   |  | |||   |\\++-+++-+/ |    | |/+-++--+-\\ \\+++--++--/|||||| | ||  || |||| ",
                    "|      |||  | |||| | |     ||||||||  | ||| |||    |||  ||| |      || |  |   |  | |||   |/++-+++-+--+----+-+++-++--+-+--+++\\ ||   |||||| | ||  || |||| ",
                    "|      |||  | |||| | |     |||||||\\--+-+++-+++----+++--+++-+------++-+--+---+--+-+++---++++-+++-+--+----+-+++-++--+-+--++++-+/   |||||| | ||  || |||| ",
                    "|      |||  | |||| | |     |||||||   | |\\+-+++----+++--+++-+------++-+--+---+--+-+++---++++-+++-+--+----+-+++-/|  | |  |||| |    \\+++++-+-+/  || |||| ",
                    "|      |||  | |||| \\-+-----+++++++---+-+-+-+++----+++--+++-+------++-+--+---+--+-+++---++/| ||| |  \\----+-+++--+--+-+--++++-+-----+++++-+-+---++-/||| ",
                    "|      |||  | ||\\+---+-----+++++++---+-+-+-+++----+++--+++-+------++-+--+---+--+-+++---/| | ||| \\-------+-+++--+--+-+--++++-/     ||||| | |   ||  ||| ",
                    "|      |||  | || | /-+-----+++++++--\\| | | |\\+----+++--/|\\-+------++-+--+---+--+-+++----+-+-+++---------/ |||  |  | |  ||||       |||||/+-+---++-\\||| ",
                    "|      |||  | || | | |     |||||||  v| |/+-+-+----+++---+--+------++-+--+---+--+-+++----+-+-+++-----------+++--+--+-+\\ ||||       ||||||| |   || |||| ",
                    "|      |||  | || | | |     |||||||  || ||| | |    |||  /+--+------++-+--+---+--+-+++----+-+-+++-----------+++--+-\\| || ||||       ||||||| |   || |||| ",
                    "|      |||  | || | | |     |||||||  || ||| | |    |||  ||  |      || |  |   |  | |||    | | |||           |||  | || || ||||       ||||||| |   || |||| ",
                    "|      |||  | || | | |     |||\\+++--++-+++-+-+----+++--++--+------++-+--+---+--+-+++----+-+-+++----<------+++--+-++-++-++++-------+++++++-/   || |||| ",
                    "|      |||  | || | | |     ||| |||  || ||| | |    |||  ||  |      || |  |   |  | |||    | | |||           |||  | || || ||||      /+++++++--\\  || |||| ",
                    "|      |||  | || | | |     ||| |||  || ||| | |    |||  ||  |      || |  |   |  | |||    \\-+-+++---->------+++--+-++-++-+++/      ||||||||  |  || |||| ",
                    "|      |||  | || | | |     ||| |\\+--++-+++-+-+----+++--++--+------++-+--+---+--+-+++------+-+/\\-----------+++--+-++-++-+++-------+++/||||  |  || |||| ",
                    "|      |||  | || | | |     ||| | |  || ||| | |    |||  ||  |      || | /+---+--+-+++------+-+--------\\    |||  | || || |||       ||| ||||  |  || |||| ",
                    "|      |||  | || | |/+-----+++-+-+\\ || ||| | |    |||  ||  |      || | ||   \\--+-+++------+-+--------+----+++--+-++-++-+++-------+++-+/||  |  || |||| ",
                    "|      |||  | |\\-+-+++-----+++-+-++-+/ ||| | |    |||  ||  |      || | ||      | |||      | |        |    |||  | || || |||       ||| | ||  |  || |||| ",
                    "|      |||  | |  | |||     ||| ^ || |  ||| | |    |||  ||  |      || | ||      | |||      | |        |    |||  | || || |||       ||| | ||  |  || |||| ",
                    "|      |||  | |  | |||     ||| | || |  |\\+-+-+----+++--++--+------++-+-++------+-+++------+-+--------+----+++--+-++-+/ |||       ||| | ||  |  || |||| ",
                    "|      |||  | |  | |||     ||| | || |  | | | |    |\\+--++--+------++-+-++------+-+/|      | |        |    |||  | || |  |||       ||| | ||  |  || |||| ",
                    "\\------+++--+-+--+-+++-----+++-+-++-+--+-+-/ |    | |  ||  |      || | ||      | | |      | |        |    |\\+--+-++-/  |||       |\\+-+-++--+--/| |||| ",
                    "       |||  | |  | |||     \\++-+-++-+--+-+---+----+-/  ||  |      || | |\\------+-+-+------+-+--------+----+-+--+-++----+++-------+-+-+-+/  |   | |||| ",
                    "       |||  | |  | |||      || | || |  | |   |    |    ||  |      || | |       | | |      | |        |    | |  | ||    |||       | | | |   |   | |||| ",
                    "       |||  | |  | |||      || | || |  | |   |    |    ||  |      || | |       | | |      | |        |    | \\--+-++----+++-------+-+-+-+---+---+-++/| ",
                    "       |||  | \\--+-+++------++-+-++-+--+-+---+----+----++--+------++-+-+-------+-+-+------+-+--------+----+----+-++----/||       | | | |   |   | || | ",
                    "    /--+++--+----+-+++------++-+-++-+--+-+---+----+----++-\\|      || | |       | | |      | |        |    |    | ||     ||       | | | | /-+---+-++-+\\",
                    "    |  |||  |    | \\++------++-+-++-/  | |   |    |    \\+-++------++-+-+-------+-+-+------+-+--------+----+----+-/|     ||       | | | | | |   | || ||",
                    "    |  |||  |    |  ||      || | ||    | |   |    |     | ||      || | |       | | \\------+-/        |    |    |  |     ||       | | | | | |   | || ||",
                    "    |  |||  |    |  ||      || | ||    | |   |    |     | ||      || | |       |/+--------+----------+----+----+\\ |     ||       | | | | | |   | || ||",
                    "    |  ||\\--+----+--++------++-+-++----+-+---+----+-----+-++------/\\-+-+-------+++--------/          |    |    || |     ||       | | | | | |   | || ||",
                    "    |  \\+---+----+--++------++-+-/|    | | /-+----+-----+-++---------+-+-------+++-------------------+---<+----++-+--\\  ||       | | | | | |   | || ||",
                    "    |   \\---+----+--++------+/ \\--+----+-+-+-+----+-----+-++---------+-+-------+++-------------------+----+----++-+--+--/|       | | | | | |   | || ||",
                    "    |       |    |  ||      |     |    | \\-+-+----+-----+-++---------+-+-------+++-------------------+----+----++-+--+---+--->---+-+-+-+-+-+---/ || ||",
                    "    |       |    |  |\\------+-----+----+---+-+----+-----+-++---------+-+-------/|\\-------------------+----+----++-/  |   |       | | | | | |     || ||",
                    "    |       \\----+--+-------+-----+----+---+-/    |     \\-++---------+-+--------+--------------------+----+----++----+---/       | | | \\-+-+-----/| ||",
                    "    |            |  |       \\-----+----+---+------+-------++---------+-+--------+--------------------+----+----++----+-----------+-/ |   | |      | ||",
                    "    |            |  |             |    |   |      |       ||         | |        |                    |    |    ||    |           |   |   | |      | ||",
                    "    \\------------+--+-------------+----+---+------+-------/|         | \\--------+--------------------/    |    ||    |           \\---+---+-/      | ||",
                    "                 |  \\-------------/    |   |      |        |         \\----------+-------------------------/    ||    |               |   |        | ||",
                    "                 |                     \\---+------+--------+--------------------+------------------------------/|    |               |   |        | ||",
                    "                 |                         |      |        \\--------------------+-------------------------------+----+---------------+---+--------/ ||",
                    "                 \\-------------------------+------/                             \\-------------------------------/    |               |   \\----------+/",
                    "                                           \\-------------------------------------------------------------------------/               \\--------------/ "
    };
    
    /*static String[] input = new String[]{
            "/->-\\        ",
                    "|   |  /----\\",
                    "| /-+--+-\\  |",
                    "| | |  | v  |",
                    "\\-+-/  \\-+--/",
                    "  \\------/   "
    };*/

    static TrackItem[][] grid;

    public static void main(String[] args){
        TrackItem[][] grid = new TrackItem[input.length][];
        for(int i=0;i<input.length;i++){
            grid[i] = new TrackItem[input[i].length()];
            byte[] bytes = input[i].getBytes();
            for(int j=0;j<bytes.length;j++){
                if(bytes[j]=='<'||bytes[j]=='>')
                    grid[i][j] = new TrackItem('-', new Cart(0, bytes[j]=='<' ? -1 : 1));
                else if(bytes[j]=='^'||bytes[j]=='v')
                    grid[i][j] = new TrackItem('|', new Cart(bytes[j]=='^' ? -1 : 1, 0));
                else
                    grid[i][j] = new TrackItem((char)bytes[j], null);
            }
        }

        boolean hasCrash;
        int x=-1, y=-1;
        do{
            hasCrash = false;
            for(int i=0;i<grid.length;i++){
                for(int j=0;j<grid[i].length;j++){
                    if(!grid[i][j].isOccupied() || grid[i][j].getCart().hasMoved())continue;

                    Cart cart = grid[i][j].getCart();
                    //System.out.println(i + " " +j);
                    TrackItem nextField = grid[i+cart.getCurrentDirectionY()][j+cart.getCurrentDirectionX()];

                    if(nextField.isOccupied()){
                        x = j+cart.getCurrentDirectionX();
                        y = i+cart.getCurrentDirectionY();
                        hasCrash = true;
                        break;
                    }

                    grid[i][j].setCart(null);
                    nextField.setCart(cart);
                    cart.adjustDirection(nextField.getTrack());
                    cart.setHasMoved(true);
                }
                if(hasCrash) break;
            }
            if(!hasCrash)
                for(int i=0;i<grid.length;i++)
                    for(int j=0;j<grid[i].length;j++)
                        if(grid[i][j].isOccupied())
                            grid[i][j].getCart().setHasMoved(false);

        }while(!hasCrash);
        System.out.println(x + " " +y);
    }
}
