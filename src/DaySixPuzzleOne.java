import support.Field;

public class DaySixPuzzleOne {
    
    static String[] input= new String[]{
            "156,193",
"81,315",
"50,197",
"84,234",
"124,162",
"339,345",
"259,146",
"240,350",
"97,310",
"202,119",
"188,331",
"199,211",
"117,348",
"350,169",
"131,355",
"71,107",
"214,232",
"312,282",
"131,108",
"224,103",
"83,122",
"352,142",
"208,203",
"319,217",
"224,207",
"327,174",
"89,332",
"254,181",
"113,117",
"120,161",
"322,43",
"115,226",
"324,222",
"151,240",
"248,184",
"207,136",
"41,169",
"63,78",
"286,43",
"84,222",
"81,167",
"128,192",
"127,346",
"213,102",
"313,319",
"207,134",
"154,253",
"50,313",
"160,330",
"332,163"
    };
    
    /*static String[] input = new String[]{
            "1,1",
"1,6",
"8,3",
"3,4",
"5,5",
"8,9"
    };*/

    public static void main(String[] args){
        int size = 360;
        //int size = 10;
        Field[][] grid = new Field[size][size];
        for(int i=0;i<grid.length;i++)
            for(int j=0;j<grid[0].length;j++)
                grid[i][j] = new Field(1000000, "draw", -1);
        for(int i=0;i<input.length;i++){
            String line = input[i];
            int x = Integer.parseInt(line.substring(0,line.indexOf(",")));
            int y = Integer.parseInt(line.substring(line.indexOf(",")+1));
            String name = getStringForInput(i);
            grid[y][x] = new Field(0, name, i);
            updateDistances(grid, x, y, name.toLowerCase(), i);
        }
        boolean[] infinite = new boolean[input.length];
        for(int i=0;i<grid.length;i++){
            if(grid[0][i].getNumber() != -1)infinite[grid[0][i].getNumber()] = true;
            if(grid[grid.length-1][i].getNumber() != -1)infinite[grid[grid.length-1][i].getNumber()] = true;
            if(grid[i][0].getNumber() != -1)infinite[grid[i][0].getNumber()] = true;
            if(grid[i][grid.length-1].getNumber() != -1)infinite[grid[i][grid.length-1].getNumber()] = true;
        }

//        for (int i=0;i<input.length;i++)
//            System.out.println((i+1)+" infinite "+infinite[i]);

        //printMatrix(grid);

        int[] numberOfFields = new int[input.length];
        for(int i=0;i<grid.length;i++)
            for(int j=0;j<grid[0].length;j++)
                if(grid[i][j].getNumber() != -1)
                    numberOfFields[grid[i][j].getNumber()]++;

        int max = -1;
        int maxNumber = -1;

        for(int i=0;i<input.length;i++){
            if(numberOfFields[i] > max && !infinite[i]){
                max = numberOfFields[i];
                maxNumber = i+1;
            }
        }

        System.out.println("Max is "+max+" bij veld "+maxNumber);
    }

    private static void updateDistances(Field[][] grid, int x, int y, String name, int number){
        for(int i=0;i<grid.length;i++){
            for(int j=0;j<grid[0].length;j++) {
                int distance = Math.abs(x-j)+Math.abs(y-i);
                if(grid[i][j].getDistance() > distance){
                    grid[i][j].setDistance(distance);
                    grid[i][j].setItem(name);
                    grid[i][j].setNumber(number);
                }
                else if(grid[i][j].getDistance() == distance && !name.toUpperCase().equals(grid[i][j].getItem())){
                    grid[i][j].setItem("draw");
                    grid[i][j].setNumber(-1);
                }
            }
        }
    }

    private static String getStringForInput(int number){
        return ""+(char)('A'+number);
        //if(number<26) return "A"+(char)('A'+number);
        //return "B"+(char)('A'+(number-26));
    }

    public static void printMatrix(Field[][] matrix){
        for(int i=0;i<matrix.length;i++){
            for(int j=0;j<matrix[i].length;j++){
                if(matrix[i][j].getItem().equals("draw"))
                    System.out.print(".");
                else
                    System.out.print(matrix[i][j].getItem());
            }
            System.out.println();
        }
    }
}
