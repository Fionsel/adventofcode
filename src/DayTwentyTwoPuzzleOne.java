public class DayTwentyTwoPuzzleOne {
    static int depth = 10914;
    static int targetx = 9;
    static int targety = 739;
    static int xfactor = 16807;
    static int yfactor = 48271;
    static int erosionLevelIndex = 20183;

    public static void main(String[] args){
        int[][] erosionLevel = new int[targety+1][targetx+1];
        for(int j=0;j<targetx+1;j++)erosionLevel[0][j]=(xfactor*j+depth)%erosionLevelIndex;
        for(int i=0;i<targety+1;i++)erosionLevel[i][0]=(yfactor*i+depth)%erosionLevelIndex;
        for(int i=1;i<targety+1;i++)for(int j=1;j<targetx+1;j++)erosionLevel[i][j]=(erosionLevel[i][j-1]*erosionLevel[i-1][j]+depth)%erosionLevelIndex;
        erosionLevel[targety][targetx]=depth;
        int[][] type = new int[targety+1][targetx+1];
        for(int i=0;i<targety+1;i++)for(int j=0;j<targetx+1;j++)type[i][j]=erosionLevel[i][j]%3;
        int sum=0;
        for(int i=0;i<targety+1;i++)for(int j=0;j<targetx+1;j++)sum+=type[i][j];
        System.out.println(sum);
    }
}
