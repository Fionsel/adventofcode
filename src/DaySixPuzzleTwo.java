public class DaySixPuzzleTwo {
    static String[] input= new String[]{
            "156,193",
            "81,315",
            "50,197",
            "84,234",
            "124,162",
            "339,345",
            "259,146",
            "240,350",
            "97,310",
            "202,119",
            "188,331",
            "199,211",
            "117,348",
            "350,169",
            "131,355",
            "71,107",
            "214,232",
            "312,282",
            "131,108",
            "224,103",
            "83,122",
            "352,142",
            "208,203",
            "319,217",
            "224,207",
            "327,174",
            "89,332",
            "254,181",
            "113,117",
            "120,161",
            "322,43",
            "115,226",
            "324,222",
            "151,240",
            "248,184",
            "207,136",
            "41,169",
            "63,78",
            "286,43",
            "84,222",
            "81,167",
            "128,192",
            "127,346",
            "213,102",
            "313,319",
            "207,134",
            "154,253",
            "50,313",
            "160,330",
            "332,163"
    };

    /*static String[] input = new String[]{
            "1,1",
            "1,6",
            "8,3",
            "3,4",
            "5,5",
            "8,9"
    };*/

    public static void main(String[] args){
        int[][] coordinates = new int[input.length][2];
        //int distanceLimit = 32;
        int distanceLimit = 10000;
        for(int i=0;i<input.length;i++){
            String line = input[i];
            coordinates[i][0] = Integer.parseInt(line.substring(0,line.indexOf(",")));
            coordinates[i][1] = Integer.parseInt(line.substring(line.indexOf(",")+1));
        }
        int size = 360;
        //int size = 10;
        int[][] grid = new int[size][size];
        for(int i=0;i<size;i++)for(int j=0;j<size;j++)for(int k=0;k<coordinates.length;k++)grid[i][j]+=Math.abs(coordinates[k][0]-j)+Math.abs(coordinates[k][1]-i);

        for(int i=0;i<grid.length;i++){
            if(grid[0][i]< distanceLimit)System.out.println(0+" "+i);
            if(grid[grid.length-1][i]< distanceLimit)System.out.println((size-1)+" "+i);
            if(grid[i][0] < distanceLimit)System.out.println(i+" "+0);
            if(grid[i][grid.length-1] < distanceLimit)System.out.println(i+" "+(size-1));
        }

        int fields = 0;
        for(int i=0;i<size;i++)for(int j=0;j<size;j++)if(grid[i][j]<distanceLimit)fields++;
        System.out.println(fields);
    }
}
