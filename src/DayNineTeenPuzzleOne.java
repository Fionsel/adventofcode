public class DayNineTeenPuzzleOne extends  DaySixteenPuzzleOne{
    
    static String[] input = new String[]{
        "addi 4 16 4",
        "seti 1 7 1",
        "seti 1 8 2",
        "mulr 1 2 3",
        "eqrr 3 5 3",
        "addr 3 4 4",
        "addi 4 1 4",
        "addr 1 0 0",
        "addi 2 1 2",
        "gtrr 2 5 3",
        "addr 4 3 4",
        "seti 2 1 4",
        "addi 1 1 1",
        "gtrr 1 5 3",
        "addr 3 4 4",
        "seti 1 8 4",
        "mulr 4 4 4",
        "addi 5 2 5",
        "mulr 5 5 5",
        "mulr 4 5 5",
        "muli 5 11 5",
        "addi 3 4 3",
        "mulr 3 4 3",
        "addi 3 21 3",
        "addr 5 3 5",
        "addr 4 0 4",
        "seti 0 5 4",
        "setr 4 1 3",
        "mulr 3 4 3",
        "addr 4 3 3",
        "mulr 4 3 3",
        "muli 3 14 3",
        "mulr 3 4 3",
        "addr 5 3 5",
        "seti 0 2 0",
        "seti 0 0 4"
    };
    
    /*static String[] input = new String[]{
            "seti 5 0 1",
            "seti 6 0 2",
            "addi 0 1 0",
            "addr 1 2 3",
            "setr 1 0 0",
            "seti 8 0 4",
            "seti 9 0 5"
    };*/

    static int registerPointer = 4;

    /*public static void main(String[] args){
        int instructionPointer=0;
        int[] register = new int[]{1,0,0,0,0,0};
        int counter = 0;
        while(instructionPointer >= 0 && instructionPointer < input.length){
            String line = input[instructionPointer];
            register[registerPointer]=instructionPointer;
            String[] instructions = line.split(" ");
            int A = Integer.parseInt(instructions[1]);
            int B = Integer.parseInt(instructions[2]);
            int C = Integer.parseInt(instructions[3]);
            switch (instructions[0]){
                case "addr":
                    addr(register, A, B, C);
                    break;
                case "addi":
                    addi(register, A, B, C);
                    break;
                case "setr":
                    setr(register, A, B, C);
                    break;
                case "seti":
                    seti(register, A, B, C);
                    break;
                case "muli":
                    muli(register, A, B, C);
                    break;
                case "mulr":
                    mulr(register, A, B, C);
                    break;
                case "gtir":
                    gtir(register, A, B, C);
                    break;
                case "gtrr":
                    gtrr(register, A, B, C);
                    break;
                case "gtri":
                    gtri(register, A, B, C);
                    break;
                case "eqir":
                    eqir(register, A, B, C);
                    break;
                case "eqri":
                    eqri(register, A, B, C);
                    break;
                case "eqrr":
                    eqrr(register, A, B, C);
                    break;
                case "borr":
                    borr(register, A, B, C);
                    break;
                case "bori":
                    bori(register, A, B, C);
                    break;
                case "bani":
                    bani(register, A, B, C);
                    break;
                case "banr":
                    banr(register, A, B, C);
                    break;
            }
            //for(int number: register)
            //    System.out.print(number+" ");
            //System.out.println();
            if(++counter % 1000000 == 0){
                for(int number: register)
                    System.out.print(number+" ");
                System.out.println();
            }
            instructionPointer = register[registerPointer];
            instructionPointer++;
            //System.out.println(instructionPointer);
            //System.out.println();
        }
        for(int number: register)
            System.out.print(number+" ");
    }*/

    //part 2
    //or just cheat, figure out online that it sums the divisors and code that
    public static void main(String[] args){
        int sum=0;
        for(int i=1;i<=10551345;i++){
            if(10551345%i == 0)sum+=i;
        }
        System.out.println(sum);
    }
}
