import java.util.LinkedList;
import java.util.Vector;

public class DayNinePuzzleOne {

    static String input = "446 players; last marble is worth 7152200 points";

    //static String input = "10 players; last marble is worth 1618 points: high score is 8317";
    //static String input = "13 players; last marble is worth 7999 points: high score is 146373";
    //static String input = "17 players; last marble is worth 1104 points: high score is 2764";
    //static String input = "21 players; last marble is worth 6111 points: high score is 54718";
    //static String input = "30 players; last marble is worth 5807 points: high score is 37305";

    public static void main(String[] args){
        int numPlayers = Integer.parseInt(input.substring(0, input.indexOf(" ")));
        int numMarbles = Integer.parseInt(input.substring(input.indexOf("worth")+6, input.indexOf(" points")));

        long[] scores = new long[numPlayers];
        //LinkedList<Integer> marbles = new LinkedList<>();
        Vector<Integer> marbles = new Vector<>(numMarbles);
        marbles.add(0);
        int position = 0;
        for(int i=1;i<=numMarbles;i++){
            if(i%23==0){
                scores[(i-1)%numPlayers]+=i;
                int positionToRemove = (position-7+marbles.size())%marbles.size();
                scores[(i-1)%numPlayers]+=marbles.remove(positionToRemove);
                position = positionToRemove;
            }
            else{
                int newPosition = i==1 ? 1 : (position+2) % marbles.size();
                marbles.add(newPosition, i);
                position = newPosition;
            }
        }
        long max = -1;
        for(int i=0;i<numPlayers;i++)
            if(scores[i] > max)
                max = scores[i];
        System.out.println(max);
    }
}
